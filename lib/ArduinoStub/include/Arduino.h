#if !defined(FAKE_ARDUINO_H)
#define FAKE_ARDUINO_H

#include "avr/interrupt.h"
#include "avr/io.h"

#define CHANGE (0)
#define F(x) (x)

#define min(x, y) ((x) < (y) ? (x) : (y))
#define max(x, y) ((x) > (y) ? (x) : (y))

void attachInterrupt(...);
int digitalPinToInterrupt(...);
int word(int a, int b);
void interrupts();

struct HardwareSerial
{
    void begin(...) {}
    void end(...) {}
    int available(...) { return 0; }
    int read(...) { return 0; }
    void print(...) {}
    void println(...) {}
    void write(...) {}
};
extern HardwareSerial Serial;
extern HardwareSerial Serial2;

#endif // FAKE_ARDUINO_H