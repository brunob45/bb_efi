#if !defined(FAKE_EEPROM_H)
#define FAKE_EEPROM_H

struct FakeEEPROM
{
    int read(...) { return 0; }
    void write(...){};
    void update(...){};
};
extern FakeEEPROM EEPROM;

#endif // FAKE_EEPROM_H