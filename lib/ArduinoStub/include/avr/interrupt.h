#if !defined(FAKE_INTERRUPT_H)
#define FAKE_INTERRUPT_H

#define ISR(x) void x()

void cli();
void sei();

#endif // FAKE_INTERRUPT_H