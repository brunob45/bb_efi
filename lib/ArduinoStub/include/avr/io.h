#if !defined(FAKE_IO_H)
#define FAKE_IO_H

#include <stdint.h>

#define TCNT3 (0)

extern uint8_t SREG;
extern uint16_t SP;

#endif // FAKE_IO_H