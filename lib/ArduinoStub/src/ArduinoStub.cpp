#include "Arduino.h"
#include "EEPROM.h"

HardwareSerial Serial;
HardwareSerial Serial2;
FakeEEPROM EEPROM;
uint8_t SREG;
uint16_t SP;
uint8_t _end;

volatile unsigned long timer0_millis;

void attachInterrupt(...) {}
int digitalPinToInterrupt(...) { return 0; }
int word(int a, int b)
{
    a &= 0xff;
    b &= 0xff;
    return (a << 8) | b;
}
void interrupts() { sei(); }

void cli() {}
void sei() {}