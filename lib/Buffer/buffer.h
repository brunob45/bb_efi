#if !defined(MY_BUFFER_H)
#define MY_BUFFER_H

#include <stdint.h>

template <typename T, int N>
class Buffer
{
    static_assert(N && !((N & (N - 1))), "N must be a power of 2.");

private:
    T buffer[N];
    uint8_t head;
    uint8_t tail;
    bool is_full;

public:
    Buffer();
    T get();
    void put(T);
    bool full();
    bool empty();
};

#include "buffer_impl.h"

#endif // MY_BUFFER_H
