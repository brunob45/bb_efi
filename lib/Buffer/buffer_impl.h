#if !defined(MY_BUFFER_IMPL_H)
#define MY_BUFFER_IMPL_H

template <typename T, int N>
Buffer<T, N>::Buffer()
    : head(0), tail(0), is_full(false)
{
}

template <typename T, int N>
T Buffer<T, N>::get()
{
    T item;

    if (!empty())
    {
        item = buffer[tail];
        is_full = false;
        tail = (tail + 1) % N;
    }

    return item;
}

template <typename T, int N>
void Buffer<T, N>::put(T item)
{
    buffer[head] = item;

    head = (head + 1) % N;

    if (is_full)
    {
        tail = head;
    }
    else
    {
        is_full = (tail == head);
    }
}

template <typename T, int N>
bool Buffer<T, N>::empty()
{
    return (!is_full && (head == tail));
}

template <typename T, int N>
bool Buffer<T, N>::full()
{
    return (is_full && (head == tail));
}

#endif // MY_BUFFER_IMPL_H