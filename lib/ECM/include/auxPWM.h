#if !defined(AUX_PWM_H)
#define AUX_PWM_H

#include <stdint.h>

void auxPwmInit();
void setDuty(uint8_t duty);

#endif // AUX_PWM_H