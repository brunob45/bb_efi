#if !defined(CALCS_H)
#define CALCS_H

#include <stdint.h>

#include "table.h"
#include "tasks.h"

void doCalcs();

uint16_t getPW(uint16_t pw, uint16_t cor, uint8_t n_squirts, uint16_t ot);
uint16_t getMaxPw(uint8_t teeth_per_event);
uint16_t getCor(uint16_t wue, uint16_t ase);
int16_t getAdv(Table3d* tbl_adv);
uint16_t getAfrTarget(Table3d* tbl_afr);
uint16_t applyAfrTarget(uint16_t pw, uint16_t target);

#endif // CALCS_H
