#if !defined(COMMS_H)
#define COMMS_H

#include "tasks.h"

class HardwareSerial;

struct SerialCmdInfoStruct
{
    bool send_pending;
    bool recv_pending;
    bool burn_pending;
    void (*pending_command)(HardwareSerial* serial);
    uint16_t offset;
    uint16_t size;
    uint16_t index;
    uint8_t* current_page;
    uint8_t need_burn;
    uint8_t page_index;
};

void commsInit();
void checkComms();

#endif // COMMS_H