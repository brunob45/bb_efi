#if !defined(DECODER_H)
#define DECODER_H

#include <stdint.h>

#include "global_objects.h"
#include "hardware.h"

#define MINUTE_IN_MICROS 60000000UL

#define WHEEL_TEETH 4
#define DEGREES_BY_CYCLE 7200
#define DEGREES_BY_TOOTH (DEGREES_BY_CYCLE / WHEEL_TEETH)

#define SPEED_CRANK 0
#define SPEED_CAM 1

#define WHEEL_SPEED SPEED_CAM

#if WHEEL_SPEED == SPEED_CAM
#define TEETH_PER_CYCLE (WHEEL_TEETH / 2)
#else
#define TEETH_PER_CYCLE (WHEEL_TEETH)
#endif

#define TICKS_TO_RPM(ticks) ((US_TO_TICKS(MINUTE_IN_MICROS) / TEETH_PER_CYCLE) / (ticks))
#define RPM_TO_TICKS(rpm) TICKS_TO_RPM(rpm)

extern volatile uint8_t current_tooth_vg;
extern volatile tick_t last_trigger_delta_vg;
extern volatile tick_t last_trigger_accel_vg;
extern volatile tick_t last_trigger_time_vg;

void decoderInit();
void decoderReset();
bool updateDecoderInfo(DecoderInfo info);
uint16_t getRpm();
int32_t ticksUntilAngle(int16_t angle, uint8_t teeth_per_event);

inline tick_t getLastTriggerDelta()
{
    AtomicBlock atomic;
    tick_t trigger_delta = last_trigger_delta_vg;
    atomic.release();

    return trigger_delta;
}

inline tick_t getLastTriggerError()
{
    AtomicBlock atomic;
    tick_t trigger_error = last_trigger_accel_vg;
    atomic.release();

    return trigger_error;
}

inline uint16_t ticksToRPM(tick_t ticks)
{
    uint16_t rpm = 0;
    if (ticks > RPM_TO_TICKS(18000)) // Set 18000 as RPM max
    {
        rpm = TICKS_TO_RPM(ticks);
    }
    return rpm;
}

inline int32_t angleToTicks(const int32_t angle)
{
    return (angle * getLastTriggerDelta()) / DEGREES_BY_TOOTH;
}

inline uint16_t getCrankAngle()
{
    AtomicBlock atomic;
    tick_t timer_delta = TIME - last_trigger_time_vg;
    tick_t trigger_delta = last_trigger_delta_vg;
    uint8_t trigger_tooth = current_tooth_vg / 2;
    atomic.release();

    return (DEGREES_BY_TOOTH * trigger_tooth) + ((DEGREES_BY_TOOTH * timer_delta) / trigger_delta);
}

#endif // DECODER_H
