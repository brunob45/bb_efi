#if !defined(GLOBALS_H)
#define GLOBALS_H

#include "hardware.h"
#include "storage.h"
#include "table.h"

class AtomicFlag
{
    bool flag;

public:
    AtomicFlag() : flag(false) {}
    void Set() { flag = true; }
    bool Get() const { return flag; }
    void Reset() { flag = false; }
    bool GetAndReset()
    {
        AtomicBlock atomic;
        const bool b = Get();
        Reset();
        atomic.release();
        return b;
    }
};

struct DecoderInfo
{
    uint16_t trigger_time;
    bool primaryIsHigh : 1;
    bool secondaryIsHigh : 1;
};

extern AtomicFlag updateMAP;

extern Buffer<DecoderInfo, 2> DecoderBuffer;

extern DigitalIO io_fuel_pump;
extern DigitalIO io_ecl;
extern DigitalIO io_idle;
extern DigitalIO io_fan;

extern TableAxis axis_brv;
extern TableAxis axis_rpm;
extern TableAxis axis_load;
extern TableAxis axis_clt;

extern TableAxis axis_limiter_load;
extern TableAxis axis_limiter_rpm;
extern TableAxis axis_launch_load;
extern TableAxis axis_launch_rpm;

extern TableAxis axis_accel_mapdot;
extern TableAxis axis_accel_rpm;

extern TableAxis axis_mat;

extern TableAxis axis_idleadv;

extern Table1d calib_clt;
extern Table1d calib_mat;

extern Table2d table_dwell;
extern Table2d table_open_time;
extern Table2d table_cranking_fuel;
extern Table2d table_wue;
extern Table2d table_ase;
extern Table2d table_idle;

extern Table2d table_accel_mapdot;
extern Table2d table_accel_rpm;

extern Table2d table_airden;
// extern Table2d table_coldadv;
extern Table2d table_idleadv;

extern Table3d table_pw;
extern Table3d table_advance;
extern Table3d table_limiter;
extern Table3d table_launch;
extern Table3d table_afrtgt;

struct AseStruct
{
    uint8_t taper;
    uint8_t last_update;

    void init()
    {
        current_status.enrich_ase = 1000 + table_ase.getValue();
        taper = 1000 / tuning_settings.fuel.ase_taper;
        current_status.engine.ase = true;
        last_update = 0;
    }
    void update()
    {
        current_status.enrich_ase -= 1;
        if (current_status.enrich_ase <= 1000)
        {
            current_status.enrich_ase = 1000;
            current_status.engine.ase = false;
        }
    }
};
extern AseStruct ase;

#endif // GLOBALS_H