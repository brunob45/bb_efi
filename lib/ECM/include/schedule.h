#if !defined(SCHEDULE_H)
#define SCHEDULE_H

#include <stdint.h>

#include "hardware.h"

class Schedule
{
public:
    typedef enum ScheduleStatusEnum : uint8_t
    {
        OFF,
        PENDING,
        RUNNING
    } ScheduleStatus;

    ScheduleStatus status;

    tick_t duration;
    tick_t next_compare;
    bool has_pending;
    tick_t time_begin;
    uint16_t actual_pw;
    uint16_t actual_period;

    TimerAbstractStruct* timer;

    Schedule(TimerAbstractStruct* _timer) : timer(_timer) {}
    void SetFromTicks(tick_t start_ticks, tick_t duration_ticks);
    void SetFromAngle(int16_t trigger_angle, uint16_t duration_us, uint8_t teeth_per_event);
    void InterruptHandler(bool (*allow)());
};

extern Schedule injSchedule1;
extern Schedule injSchedule2;
extern Schedule ignSchedule1;
extern Schedule ignSchedule2;

void scheduleInit();

#endif // SCHEDULE_H
