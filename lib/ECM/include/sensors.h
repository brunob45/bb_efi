#if !defined(SENSORS_H)
#define SENSORS_H

#define BRV_VALUE(x) ((uint16_t)((x)*10.0))
#define MAP_VALUE(x) ((uint16_t)((x)*10.0))
#define TPS_VALUE(x) ((uint16_t)((x)*1.0))
#define EGO_VALUE(x) ((uint16_t)((x)*1024.0))
#define CLT_VALUE(x) ((int16_t)((x)*10.0))
#define MAT_VALUE(x) ((int16_t)((x)*10.0))

#define EGOTGT_VALUE(x) ((uint16_t)((x)*512.0))

enum class SensorIndex : uint8_t
{
    MAP,
    TPS,
    EGO,
    BRV,
    CLT,
    MAT
};

void updateMapSensor(uint16_t adc_reading);
void updateTpsSensor(uint16_t adc_reading);
void updateEgoSensor(uint16_t adc_reading);
void updateBrvSensor(uint16_t adc_reading);
void updateCltSensor(uint16_t adc_reading);
void updateMatSensor(uint16_t adc_reading);

void sensorsInit();
void updateSensors(SensorIndex index, uint16_t adc_value);
void updateMapValue();

#endif // SENSORS_H