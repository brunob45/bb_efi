#if !defined(STATE_MACHINE_H)
#define STATE_MACHINE_H

#include <stdint.h>

void updateEngineState();

#endif // STATE_MACHINE_H