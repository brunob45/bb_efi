#if !defined(STORAGE_H)
#define STORAGE_H

#include <stdint.h>

#include "current_status.h"

class HardwareSerial;

#define CALIB_SIZE (1 << 5)
#define CALIB_INDEX(x) ((x) >> 5)

struct StorageBurnInfoStruct
{
    uint8_t page_index;
    uint8_t* current_page;
    uint16_t offset;
    uint16_t size;
    uint16_t index;
};

struct PinDefStruct
{
    uint8_t pin : 6;
    uint8_t inv : 1;
    uint8_t : 0;
};

struct PageFuelStruct
{
    uint16_t inj1_angle;
    uint16_t inj2_angle;
    uint8_t req_fuel;

    uint8_t n_squirts : 3;
    bool flash_lock : 1;
    bool idle_enable : 1;
    bool fuel_enable : 1;
    bool spark_enable : 1;
    bool launch_enable : 1;
    uint8_t : 0;

    int16_t crank_angle_offset;
    uint8_t cranking_fuel[8];
    struct
    {
        uint8_t brv : 4;
        uint8_t map : 4;
        uint8_t mat : 4;
        uint8_t clt : 4;
        uint8_t tps : 4;
        uint8_t ego : 4;
        uint8_t adc6 : 4;
        uint8_t adc7 : 4;
        uint8_t adc8 : 4;
        uint8_t adc9 : 4;
    } analog_input;
    struct
    {
        uint8_t brv : 4;
        uint8_t map : 4;
        uint8_t mat : 4;
        uint8_t clt : 4;
        uint8_t tps : 4;
        uint8_t ego : 4;
    } analog_lag;
    uint8_t fuel_cut_map_on;
    uint8_t fuel_cut_delay;
    uint8_t ae_tps;
    uint16_t crank_pw_add;
    uint16_t crank_pw_max;
    uint16_t adc_debug;
    uint8_t limiter_seed;
    uint8_t limiter_hard;
    int8_t limiter_sft_ret;
    uint8_t fp_prime_time;
    int16_t brv_axis[4];
    int16_t clt_axis[8];
    uint8_t wue_values[8];
    uint8_t ase_values[8];
    uint8_t ase_taper;
    uint8_t idle_target[8];
    uint8_t flood_clear;
    uint8_t idle_duty;
    uint16_t idle_frequency;

    uint8_t serial2_baud : 2;
    uint8_t : 0;

    uint8_t fan_on_temp;
    uint8_t fan_off_temp;

    uint8_t vss_lag_factor;

    uint8_t fuel_cut_map_off;
    int16_t ae_mapdot_axis[4];
    int16_t ae_rpm_axis[4];
    uint8_t ae_add_value[4];
    uint8_t ae_mul_value[4];
    uint8_t ae_dur;
    uint8_t ae_th;
    int16_t mat_axis[8];
    uint8_t airden_values[8];
};

struct PageSparkStruct
{
    uint8_t open_time_values[4];
    uint8_t dwell_values[4];
    uint8_t idle_tps;
    uint8_t idle_map;
    uint8_t idle_rpm;
    uint8_t cranking_dwell;
    uint8_t idle_p;
    uint8_t idle_i;
    uint8_t idle_d;
    uint8_t trigger1_pin : 3;
    uint8_t trigger1_inv : 1;
    uint8_t trigger2_pin : 3;
    uint8_t trigger2_inv : 1;
    uint8_t sim_rpm;
    int16_t fixed_adv_angle;
    bool fixed_adv_enable : 1;
    bool soft_limit_spark : 1;
    bool soft_limit_fuel : 1;
    bool hard_limit_spark : 1;
    bool hard_limit_fuel : 1;
    bool sim_enable : 1;
    uint8_t : 0;
    int8_t cranking_adv;
    uint8_t cranking_rpm;
    int16_t limiter_rpm[4];
    int16_t limiter_load[4];
    uint8_t limiter_values[4 * 4];
    int16_t launch_rpm[4];
    int16_t launch_load[4];
    uint8_t launch_values[4 * 4];
    int16_t idle_adv_rpm[4];
    uint8_t idle_adv_deg[4];
};

struct PageCalibStruct
{
    uint16_t tps_axis[2];
    uint8_t tps_values[2];

    uint16_t ego_axis[2];
    uint8_t ego_values[2];

    uint16_t map_axis[2];
    uint8_t map_values[2];

    uint16_t brv_axis[2];
    uint8_t brv_values[2];

    uint16_t clt_min;                  // 3952
    uint16_t clt_max;                  // 3954
    uint16_t mat_min;                  // 3956
    uint16_t mat_max;                  // 3958
    int16_t clt_default;               // 3960
    int16_t mat_default;               // 3962
    int16_t clt_calib[CALIB_SIZE + 1]; // 3964
    int16_t mat_calib[CALIB_SIZE + 1]; // 4030
};

struct PageTablesStruct
{
    int16_t axis_rpm[16];
    int16_t axis_load[16];
    uint8_t values_ve[16 * 16];
    uint8_t values_adv[16 * 16];
};

struct PageTables2Struct
{
    uint8_t values_afr[16 * 16];
};

struct TuningSettings
{
    PageFuelStruct fuel;
    PageSparkStruct spark;
    PageTablesStruct tables;
    PageCalibStruct calib;
    PageTables2Struct tables2;
};

extern CurrentStatus current_status;
extern TuningSettings tuning_settings;

uint8_t* page2ByteArray(uint8_t n);
bool initWriteStorage(uint8_t page_index);
uint8_t doWriteStorage();
void readStorage();
void writeCalib(uint8_t n, uint8_t offset, int16_t value);
void writeAt(uint16_t offset, uint16_t value);

#endif // STORAGE_H
