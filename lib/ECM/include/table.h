#if !defined(TABLE_H)
#define TABLE_H

#include <stdint.h>

struct TableAxis
{
    uint8_t size;
    int16_t* bins;
    uint8_t bin;
    uint8_t w;

    void Update(int16_t x);
};

struct Table1d
{
    int16_t* values;
    uint16_t min;
    uint16_t max;
    int16_t def_value;

    int16_t getValue(uint16_t x);
};

struct Table2d
{
    TableAxis* axis;
    uint8_t* values;
    uint8_t multiplier;

    uint16_t getValue();
};

struct Table3d
{
    TableAxis* axis_x;
    TableAxis* axis_y;
    uint8_t* values;
    uint8_t multiplier;

    uint16_t getValue();
};

#endif // TABLE_H