#if !defined(TASKS_H)
#define TASKS_H

#include <stdint.h>

struct Context
{
    uint8_t current_task;
    uint8_t last_task;
    uint16_t current_millis;
};

typedef bool (*Task)(Context& context);

extern const Task tasks[];
extern const uint8_t NUM_TASKS;

#endif // TASKS_H