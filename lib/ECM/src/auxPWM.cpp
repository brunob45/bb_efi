#include "auxPWM.h"
#include "global_objects.h"
#include "hardware.h"

#define IDLE_TIMER() (TimerImplStruct<11>::Instance())

static bool gIsInit = false;
static tick_t gPeriod = 0;
static tick_t gDuty = 0;
static bool gIsHigh = false;

void auxPwmInit()
{
    if (tuning_settings.fuel.idle_frequency > 0)
    {
        gPeriod = US_TO_TICKS(1000000U) / tuning_settings.fuel.idle_frequency;
        IDLE_TIMER()->Init();
        gIsInit = true;
    }
}

void setDuty(uint8_t duty)
{
    if (gIsInit)
    {
        if (duty == 0)
        {
            io_idle.setLow();
            IDLE_TIMER()->DisableInterrupt();
        }
        else if (duty == 255)
        {
            io_idle.setHigh();
            IDLE_TIMER()->DisableInterrupt();
        }
        else
        {
            gDuty = ((uint32_t)gPeriod * duty) / 256U;
            IDLE_TIMER()->EnableInterrupt();
        }
    }
}

TIMER0_CO_HANDLER()
{
    if (gIsHigh)
    {
        io_idle.setLow();
        IDLE_TIMER()->Schedule(gPeriod - gDuty);
        gIsHigh = false;
    }
    else
    {
        io_idle.setHigh();
        IDLE_TIMER()->Schedule(gDuty);
        gIsHigh = true;
    }
}