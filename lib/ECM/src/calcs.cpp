#include <stdint.h>

#include "calcs.h"
#include "decoder.h"
#include "global_objects.h"
#include "hardware.h"
#include "storage.h"

static DigitalInput io_clutch(PIN_CLUTCH_num, PIN_CLUTCH_lvl);

uint16_t getPW(uint16_t pw, uint16_t cor, uint8_t n_squirts, uint16_t ot)
{
    uint32_t tmp = pw;
    tmp = ((tmp * cor) / 1024U);
    tmp /= n_squirts;
    tmp += ot;
    return (tmp > 0xffff) ? 0xffff : tmp;
}

uint16_t getMaxPw(uint8_t teeth_per_event)
{
    uint32_t trigger_delta = getLastTriggerDelta();
    uint32_t pw = 15UL * TICKS_TO_US(teeth_per_event * trigger_delta) / 16; // 15/16 = 93.75% DC
    return (pw >= 0xffff) ? 0xffff : pw;
}

uint16_t getCor()
{
    uint16_t cor = 1024U;
    if (current_status.engine.warmup)
    {
        current_status.warmup_enrich = 1000 + table_wue.getValue();
        current_status.engine.warmup = (current_status.warmup_enrich > 1000);
        cor = ((uint32_t)cor * current_status.warmup_enrich) / 1000;
    }
    if (current_status.engine.ase)
    {
        cor = ((uint32_t)cor * current_status.enrich_ase) / 1000;
    }
    current_status.airden_cor = table_airden.getValue();
    cor = ((uint32_t)cor * current_status.airden_cor) / 1024;
    return cor;
}

int16_t getAdv(Table3d* tbl_adv)
{
    int16_t adv;
    if (tuning_settings.spark.fixed_adv_enable)
    {
        adv = tuning_settings.spark.fixed_adv_angle;
    }
    else if (current_status.engine.crank)
    {
        adv = 10 * tuning_settings.spark.cranking_adv;
    }
    else
    {
        adv = tbl_adv->getValue();

        if (current_status.status1.sft_limit)
        {
            adv -= 10 * tuning_settings.fuel.limiter_sft_ret;
        }
    }
    return adv;
}

uint16_t getAfrTarget(Table3d* tbl_afr)
{
    return 384U + tbl_afr->getValue();
}

uint16_t applyAfrTarget(uint16_t pw, uint16_t target)
{
    return (512UL * pw) / target;
}

void doCalcs()
{
    static bool last_clutch_in = false;
    const bool clutch_in = io_clutch.get();

    current_status.status1.idle =
        (current_status.map > 10U * tuning_settings.spark.idle_map) &&
        (current_status.tps < tuning_settings.spark.idle_tps) &&
        (current_status.rpm < 50 * tuning_settings.spark.idle_rpm);

    if (current_status.status2.decel_fuel_cut)
    {
        if ((clutch_in) || (current_status.map >= (10U * tuning_settings.fuel.fuel_cut_map_off)))
        {
            current_status.status2.decel_fuel_cut = false;
        }
    }
    else if ((!clutch_in) && (current_status.map < (10U * tuning_settings.fuel.fuel_cut_map_on)))
    {
        current_status.status2.decel_fuel_cut = true;
    }

    current_status.status2.launch_arm =
        tuning_settings.fuel.launch_enable &&
        clutch_in &&
        (current_status.status2.launch_arm || (!last_clutch_in &&
                                               current_status.status1.idle &&
                                               !current_status.engine.warmup));

    current_status.status2.floodclear =
        current_status.engine.crank &&
        (current_status.tps >= tuning_settings.fuel.flood_clear);

    current_status.status2.clutch_in = clutch_in;
    last_clutch_in = clutch_in;

    // Ignition calcs
    if (current_status.status1.run)
    {
        if (current_status.status1.idle)
        {
            axis_idleadv.Update(current_status.rpm - current_status.idle_target);
            current_status.advance = table_idleadv.getValue();
        }
        else
        {
            current_status.advance = getAdv(&table_advance);
        }
        current_status.dwell = table_dwell.getValue();
    }

    // injection calcs
    current_status.ego_tgt = getAfrTarget(&table_afrtgt);
    current_status.total_cor = getCor();
    current_status.open_time = table_open_time.getValue();

    if (!current_status.status1.run || current_status.status2.decel_fuel_cut || current_status.status2.floodclear)
    {
        current_status.req_pw = 0;
        current_status.pw = 0;
    }
    else if (current_status.engine.crank)
    {
        current_status.req_pw = table_cranking_fuel.getValue() + current_status.cranking_pw;
        current_status.pw = current_status.req_pw; // + current_status.open_time;
    }
    else
    {
        current_status.req_pw = table_pw.getValue();

        current_status.pw = getPW(current_status.req_pw, current_status.total_cor, current_status.n_squirts, current_status.open_time + current_status.accel_enrich);
        current_status.pw = applyAfrTarget(current_status.pw, current_status.req_pw);

        uint16_t max_pw = getMaxPw(current_status.teeth_per_event);
        if (current_status.pw > max_pw) { current_status.pw = max_pw; }
    }
}
