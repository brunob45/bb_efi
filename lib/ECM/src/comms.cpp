#include <Arduino.h>

#include "comms.h"
#include "storage.h"

const char CODE_VERSION[] = "MSII - bbefi v2008";
const char CODE_DESCRIPTION[] = "BB efi";
const char PROTOCOL_VERSION[] = "001";

#define F_TO_C(f) (((f)-320) * 5 / 9)

void doCommand(HardwareSerial* serial, char command);

void command_b_burn(HardwareSerial* serial);
void command_p_send(HardwareSerial* serial);
void command_w_recv(HardwareSerial* serial);
void command_r_info(HardwareSerial* serial);
void command_t_calib(HardwareSerial* serial);

static SerialCmdInfoStruct SerialCmdInfo = {false, false, false, 0, 0, 0, 0, page2ByteArray(0), 0, 0};

void commsInit()
{
    const uint32_t baud[] = {9600, 38400, 57600, 115200};

    Serial.begin(115200);
    Serial2.begin(baud[tuning_settings.fuel.serial2_baud]);
}

void doSerialSend(HardwareSerial* serial)
{
    uint8_t size = min(16, SerialCmdInfo.size - SerialCmdInfo.index);
    while (size > 0)
    {
        serial->write(SerialCmdInfo.current_page[SerialCmdInfo.offset + SerialCmdInfo.index]);
        SerialCmdInfo.index++;
        size--;
    }
    SerialCmdInfo.send_pending = SerialCmdInfo.index < SerialCmdInfo.size;
}

void doSerialRecv(HardwareSerial* serial)
{
    uint8_t size = min(16, SerialCmdInfo.size - SerialCmdInfo.index);
    while (serial->available() && size > 0)
    {
        SerialCmdInfo.current_page[SerialCmdInfo.offset + SerialCmdInfo.index] = serial->read();
        SerialCmdInfo.index++;
        size--;
    }
    SerialCmdInfo.recv_pending = SerialCmdInfo.index < SerialCmdInfo.size;
}

void checkComms()
{
    static char current_command = 0;
    static HardwareSerial* serial = &Serial;

    if (SerialCmdInfo.send_pending)
    {
        doSerialSend(serial);
    }
    else if (SerialCmdInfo.recv_pending)
    {
        doSerialRecv(serial);
    }
    else if (SerialCmdInfo.pending_command)
    {
        SerialCmdInfo.pending_command(serial);
    }
    else if (Serial.available())
    {
        serial = &Serial;
        current_command = serial->read();
        doCommand(serial, current_command);
    }
    else if (Serial2.available())
    {
        serial = &Serial2;
        current_command = serial->read();
        doCommand(serial, current_command);
    }

    if (SerialCmdInfo.burn_pending)
    {
        uint8_t mask = doWriteStorage();
        if (mask != 0)
        {
            SerialCmdInfo.burn_pending = false;
            SerialCmdInfo.need_burn &= ~mask;
            if (SerialCmdInfo.need_burn == 0) { current_status.status1.need_burn &= false; }
        }
    }
}

void doCommand(HardwareSerial* serial, char command)
{
    switch (command)
    {
    case 'b':
        command_b_burn(serial);
        break;

    case 'C': // test communications. This is used by Tunerstudio to see whether there is an ECU on a given serial port
        serial->write(1);
        break;

    case 'F': // send serial protocol version
        serial->write(PROTOCOL_VERSION, sizeof(PROTOCOL_VERSION));
        break;

    case 'N': // Displays a new line.  Like pushing enter in a text editor
        serial->write('\n');
        break;

    case 'p':
        command_p_send(serial);
        break;

    case 'Q': // send code version
        serial->write(CODE_VERSION, sizeof(CODE_VERSION));
        break;

    case 'r':
        command_r_info(serial);
        break;

    case 'S': // send code version
        serial->write(CODE_DESCRIPTION, sizeof(CODE_DESCRIPTION));
        current_status.seconds = 0; // This is required in TS3 due to its stricter timings
        break;

    case 't':
        if (tuning_settings.fuel.flash_lock == 1)
        {
            command_t_calib(serial);
        }
        else
        {
            serial->end();
        }
        break;

    case 'w':
        command_w_recv(serial);
        break;

    default:
        break;
    }
}

// New EEPROM burn command to only burn a single page at a time
void command_b_burn(HardwareSerial* serial)
{
    if (serial->available() < 2)
    {
        SerialCmdInfo.pending_command = command_b_burn;
    }
    else
    {
        serial->read(); // CANid value, always 0
        SerialCmdInfo.page_index = serial->read();

        SerialCmdInfo.burn_pending = initWriteStorage(SerialCmdInfo.page_index);

        SerialCmdInfo.pending_command = 0;
    }
}

// New method for sending page values
void command_p_send(HardwareSerial* serial)
{
    // 6 bytes required:
    // 2 - Page identifier
    // 2 - offset
    // 2 - Length
    if (serial->available() < 6)
    {
        SerialCmdInfo.pending_command = command_p_send;
    }
    else
    {
        uint8_t a, b;

        SerialCmdInfo.send_pending = true;

        serial->read(); // CANid value, always 0
        SerialCmdInfo.page_index = serial->read();

        a = serial->read();
        b = serial->read();
        SerialCmdInfo.offset = word(b, a);

        a = serial->read();
        b = serial->read();
        SerialCmdInfo.size = word(b, a);

        SerialCmdInfo.index = 0;
        SerialCmdInfo.current_page = page2ByteArray(SerialCmdInfo.page_index);

        SerialCmdInfo.pending_command = 0;
    }
}

void command_w_recv(HardwareSerial* serial)
{
    // This means it's a new request
    // 7 bytes required:
    // 2 - Page identifier
    // 2 - offset
    // 2 - Length
    // 1 - 1st New value
    if (serial->available() < 7)
    {
        SerialCmdInfo.pending_command = command_w_recv;
    }
    else
    {
        uint8_t a, b;

        SerialCmdInfo.recv_pending = true;

        serial->read(); // CANid value, always 0
        SerialCmdInfo.page_index = serial->read();

        a = serial->read();
        b = serial->read();
        SerialCmdInfo.offset = word(b, a);

        a = serial->read();
        b = serial->read();
        SerialCmdInfo.size = word(b, a);

        SerialCmdInfo.index = 0;
        SerialCmdInfo.current_page = page2ByteArray(SerialCmdInfo.page_index);

        SerialCmdInfo.need_burn |= (1 << SerialCmdInfo.page_index);
        current_status.status1.need_burn = true;

        SerialCmdInfo.pending_command = 0;
    }
}

// New format for the optimised OutputChannels
void command_r_info(HardwareSerial* serial)
{
    if (serial->available() < 6)
    {
        SerialCmdInfo.pending_command = command_r_info;
    }
    else
    {
        uint8_t a, b;

        serial->read();     // CANid value, always 0
        a = serial->read(); // read the command

        if (a == 0x30) // Send output channels command 0x30 is 48dec
        {
            SerialCmdInfo.send_pending = true;

            a = serial->read();
            b = serial->read();
            SerialCmdInfo.offset = word(b, a);

            a = serial->read();
            b = serial->read();
            SerialCmdInfo.size = word(b, a);

            SerialCmdInfo.index = 0;
            SerialCmdInfo.current_page = (uint8_t*)(&current_status);
        }
        else
        { // No other r/ commands should be called
        }

        SerialCmdInfo.pending_command = 0;
    }
}

// Receive new Calibration info. Command structure: "t", <tble_idx> <data array>. This is an MS2/Extra command, NOT part of MS1 spec
void command_t_calib(HardwareSerial* serial)
{
    while (serial->available() < 1)
    { /*Wait*/
    }
    uint8_t table_id = serial->read();

    while (serial->available() < 2)
    { /*Wait*/
    }
    int16_t default_value = serial->read();
    default_value = word(serial->read(), default_value);

    if (table_id == 0)
    {
        writeAt(3960, F_TO_C(default_value));
    }
    else
    {
        writeAt(3962, F_TO_C(default_value));
    }

    int16_t value = 0;
    uint16_t index_min = 0;
    do
    {
        while (serial->available() < 2)
        { /*Wait*/
        }
        value = serial->read();
        value = word(serial->read(), value);
        index_min++;
    } while (default_value == value);
    writeCalib(table_id, 0, F_TO_C(value));

    if (table_id == 0)
    {
        writeAt(3952, index_min);
    }
    else
    {
        writeAt(3956, index_min);
    }

    uint16_t index_max = 0;
    bool max_found = false;
    for (uint16_t i = 1; i < 1024 - index_min; i++)
    {
        while (serial->available() < 2)
        { /*Wait*/
        }
        int16_t new_value = serial->read();
        new_value = word(serial->read(), new_value);

        if (new_value != default_value)
        {
            value = new_value;
            max_found = false;
        }
        else if (!max_found)
        {
            index_max = i + index_min;
            max_found = true;
        }

        if ((i & (CALIB_SIZE - 1)) == 0)
        {
            writeCalib(table_id, CALIB_INDEX(i), F_TO_C(value));
        }
    }
    writeCalib(table_id, CALIB_SIZE, F_TO_C(value));

    if (table_id == 0)
    {
        writeAt(3954, index_max);
    }
    else if (table_id == 1)
    {
        writeAt(3958, index_max);
    }
}
