
#include "decoder.h"
#include "storage.h"

static DigitalInput io_pri(PIN_TRIG1_num, PIN_TRIG1_lvl);
static DigitalInput io_sec(PIN_TRIG2_num, PIN_TRIG2_lvl);
static volatile bool tooth_detected_svg = false;

static volatile uint8_t pulse_skipped_svg = 0;
static const uint8_t skip_pulse_sg = 2;

volatile uint8_t current_tooth_vg = 0;
volatile tick_t last_trigger_time_vg = 0;
volatile tick_t last_trigger_delta_vg = COUNTER_MAX;
volatile tick_t last_trigger_accel_vg = 0;

static TimerImplStruct<2>& TimerLostSync = *TimerImplStruct<2>::Instance();
static TimerImplStruct<3>& TimerSimulation = *TimerImplStruct<3>::Instance();

// int16_t toothAngle[] = { 1050, 1750, 2850, 3550, 4650, 5350, 6450, 7150 };

static inline void lostSync()
{
    current_status.status1.half_sync = false;
    current_status.status1.full_sync = false;
    current_status.engine.ready = false;
    pulse_skipped_svg = 0;
}

void Timer2CaptureCallback(uint16_t trigger_time)
{
    DecoderBuffer.put({trigger_time, io_pri.get() != 0, io_sec.get() != 0});
}

bool updateDecoderInfo(DecoderInfo info)
{
    const bool primaryIsHigh = info.primaryIsHigh;
    const bool secondaryIsHigh = info.secondaryIsHigh;
    uint8_t current_tooth = current_tooth_vg;
    bool retVal = false;

    tooth_detected_svg = true;

    if (!current_status.status1.full_sync)
    {
        if (current_status.status1.half_sync)
        {
            if (primaryIsHigh)
            {
                lostSync();
            }
            else
            {
                current_tooth = secondaryIsHigh ? 7 : 3;
                current_status.status1.full_sync = true;
            }
        }
        else // not isHalfSync
        {
            if (primaryIsHigh && secondaryIsHigh)
            {
                current_status.status1.half_sync = true;
            }
        }
    }
    else // isFullSync
    {
        if (current_tooth >= 7)
        {
            current_tooth = 0;
        }
        else
        {
            current_tooth++;
        }

        if (current_tooth == 7)
        {
            if (primaryIsHigh | !secondaryIsHigh)
            {
                lostSync();
            }
        }
        else if (current_tooth & 1)
        {
            if (primaryIsHigh | secondaryIsHigh)
            {
                lostSync();
            }
            else if ((current_tooth & 0x03) == 0x01)
            {
                updateMAP.Set();
            }
        }
        else if (!primaryIsHigh)
        {
            lostSync();
        }
        else
        {
            tick_t trigger_delta = info.trigger_time - last_trigger_time_vg;
            int16_t trigger_accel = last_trigger_delta_vg - trigger_delta;

            if (current_status.engine.ready)
            {
                current_status.crank_angle_err = (current_status.crank_angle_err + trigger_accel) / 2;
                TimerLostSync.Schedule(trigger_delta);
            }
            else
            {
                const uint8_t pulse_skipped = pulse_skipped_svg + 1;
                current_status.engine.ready = (pulse_skipped >= skip_pulse_sg);
                current_status.crank_angle_err = 0;
                TimerLostSync.Schedule(COUNTER_MAX);
                pulse_skipped_svg = pulse_skipped;
            }

            last_trigger_accel_vg = (trigger_accel < 0) ? -trigger_accel : trigger_accel;
            last_trigger_delta_vg = trigger_delta;
            last_trigger_time_vg = info.trigger_time;
        }
        retVal = true;
    }
    current_tooth_vg = current_tooth;
    current_status.stack_pointer = available_ram();
    return retVal;
}

void decoderInit()
{
    if (tuning_settings.spark.sim_enable)
    {
        TimerSimulation.Init();
    }
    else
    {
        TimerCapture2Enable();
    }
    TimerLostSync.Init();
}

void decoderReset()
{
    last_trigger_delta_vg = UINT16_MAX;
    last_trigger_accel_vg = 0;
}

uint16_t getRpm()
{
    AtomicBlock atomic;
    tick_t trigger_delta = last_trigger_delta_vg;
    atomic.release();

    uint16_t rpm = 0;

    if (current_status.engine.ready && (trigger_delta != 0)) { rpm = ticksToRPM(trigger_delta); }

    return rpm;
}

int32_t ticksUntilAngle(int16_t target, uint8_t teeth_per_event)
{
    AtomicBlock atomic;
    const int32_t trigger_delta = last_trigger_delta_vg;
    const uint8_t trigger_tooth = current_tooth_vg / 2;
    const tick_t trigger_time = TIME - last_trigger_time_vg;
    atomic.release();

    const int32_t ticks_max = trigger_delta * teeth_per_event;
    const int32_t crank_angle = (trigger_delta * trigger_tooth) + trigger_time;

    int32_t ticks = angleToTicks(target) - crank_angle;

    while (ticks <= 0) { ticks += ticks_max; }
    while (ticks > ticks_max) { ticks -= ticks_max; }

    return ticks;
}

TIMER4_CO_HANDLER()
{
    if (tooth_detected_svg)
    {
        TimerLostSync.Schedule(getLastTriggerDelta());
        tooth_detected_svg = false;
    }
    else
    {
        lostSync();
        current_status.rpm = 0;
    }
}

TIMER5_CO_HANDLER()
{
    static uint8_t sim_rpm = 0;
    static tick_t ticks = 0;
    static bool primaryState = false;
    static bool secondaryState = false;

    TimerSimulation.Schedule(ticks);

    if (sim_rpm >= 4)
    {
        if (!current_status.status1.full_sync)
        {
            if (!primaryState)
            {
                secondaryState = !secondaryState;
            }
        }
        else
        {
            secondaryState = (current_tooth_vg == 6);
        }
        primaryState = !primaryState;

        DecoderBuffer.put({TIME, primaryState, secondaryState});
    }

    if (sim_rpm != tuning_settings.spark.sim_rpm)
    {
        sim_rpm = tuning_settings.spark.sim_rpm;
        if (sim_rpm < 4)
        {
            io_pri.setLow();
            io_sec.setLow();
        }
        else
        {
            ticks = RPM_TO_TICKS(50 * sim_rpm) / 2;
        }
    }
}
