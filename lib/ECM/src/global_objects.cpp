#include "global_objects.h"

AtomicFlag updateMAP;
Buffer<DecoderInfo, 2> DecoderBuffer;

AseStruct ase = {0, 0};

DigitalIO io_fuel_pump = DigitalOutput(PIN_FUEL_PUMP_num, PIN_FUEL_PUMP_lvl);
DigitalIO io_ecl = DigitalOutput(PIN_ECL_num, PIN_ECL_lvl);
DigitalIO io_idle = DigitalOutput(PIN_IDLE_num, PIN_IDLE_lvl);
DigitalIO io_fan = DigitalOutput(PIN_FAN_num, PIN_FAN_lvl);

TableAxis axis_brv = {4, tuning_settings.fuel.brv_axis, 0, 0};
TableAxis axis_rpm = {16, tuning_settings.tables.axis_rpm, 0, 0};
TableAxis axis_load = {16, tuning_settings.tables.axis_load, 0, 0};
TableAxis axis_clt = {8, tuning_settings.fuel.clt_axis, 0, 0};

TableAxis axis_limiter_load = {4, tuning_settings.spark.limiter_load, 0, 0};
TableAxis axis_limiter_rpm = {4, tuning_settings.spark.limiter_rpm, 0, 0};
TableAxis axis_launch_load = {4, tuning_settings.spark.launch_load, 0, 0};
TableAxis axis_launch_rpm = {4, tuning_settings.spark.launch_rpm, 0, 0};

TableAxis axis_accel_mapdot = {4, tuning_settings.fuel.ae_mapdot_axis, 0, 0};
TableAxis axis_accel_rpm = {4, tuning_settings.fuel.ae_rpm_axis, 0, 0};

TableAxis axis_mat = {8, tuning_settings.fuel.mat_axis, 0, 0};

TableAxis axis_idleadv = {4, tuning_settings.spark.idle_adv_rpm, 0, 0};

Table1d calib_clt = {tuning_settings.calib.clt_calib, 0, 1024, 982};
Table1d calib_mat = {tuning_settings.calib.mat_calib, 0, 1024, 371};

Table2d table_dwell = {&axis_brv, tuning_settings.spark.dwell_values, 100};
Table2d table_open_time = {&axis_brv, tuning_settings.spark.open_time_values, 100};
Table2d table_cranking_fuel = {&axis_clt, tuning_settings.fuel.cranking_fuel, 20};
Table2d table_wue = {&axis_clt, tuning_settings.fuel.wue_values, 10};
Table2d table_ase = {&axis_clt, tuning_settings.fuel.ase_values, 10};
Table2d table_idle = {&axis_clt, tuning_settings.fuel.idle_target, 25};

Table2d table_accel_mapdot = {&axis_accel_mapdot, tuning_settings.fuel.ae_add_value, 40};
Table2d table_accel_rpm = {&axis_accel_rpm, tuning_settings.fuel.ae_mul_value, 1};

Table2d table_airden = {&axis_mat, tuning_settings.fuel.airden_values, 8};
Table2d table_idleadv = {&axis_idleadv, tuning_settings.spark.idle_adv_deg, 5};

Table3d table_pw = {&axis_rpm, &axis_load, tuning_settings.tables.values_ve, 10};
Table3d table_advance = {&axis_rpm, &axis_load, tuning_settings.tables.values_adv, 5};
Table3d table_limiter = {&axis_limiter_rpm, &axis_limiter_load, tuning_settings.spark.limiter_values, 1};
Table3d table_launch = {&axis_launch_rpm, &axis_launch_load, tuning_settings.spark.launch_values, 1};
Table3d table_afrtgt = {&axis_rpm, &axis_load, tuning_settings.tables2.values_afr, 1};
