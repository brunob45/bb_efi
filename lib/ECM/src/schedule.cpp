
#include "schedule.h"

#include "decoder.h"
#include "storage.h"

DigitalIO io_inj[] = {DigitalOutput(PIN_INJ1_num, PIN_INJ1_lvl), DigitalOutput(PIN_INJ2_num, PIN_INJ2_lvl)};
DigitalIO io_ign[] = {DigitalOutput(PIN_IGN1_num, PIN_IGN1_lvl), DigitalOutput(PIN_IGN2_num, PIN_IGN2_lvl)};

Schedule injSchedule1(TimerImplStruct<PIN_INJ1_num>::Instance());
Schedule injSchedule2(TimerImplStruct<PIN_INJ2_num>::Instance());
Schedule ignSchedule1(TimerImplStruct<PIN_IGN1_num>::Instance());
Schedule ignSchedule2(TimerImplStruct<PIN_IGN2_num>::Instance());

// void Schedule::SetFromTicks(tick_t start_ticks, tick_t duration_ticks)
// {
//     AtomicBlock atomic;

//     duration = duration_ticks;

//     if (status != RUNNING)
//     {
//         timer->Schedule(start_ticks);
//         status = PENDING;
//         has_pending = false;
//     }
//     else
//     {
//         next_compare = start_ticks + timer->Counter();
//         has_pending = true;
//     }

//     atomic.release();
// }

void Schedule::SetFromAngle(int16_t trigger_angle, uint16_t duration_us, uint8_t teeth_per_event)
{
    const tick_t duration_ticks = US_TO_TICKS(duration_us);
    const int32_t end_compare = ticksUntilAngle(trigger_angle + tuning_settings.fuel.crank_angle_offset, teeth_per_event);
    int32_t start_compare = end_compare - duration_ticks;

    AtomicBlock atomic;

    duration = duration_ticks;

    if (start_compare < COUNTER_MAX)
    {
        if (start_compare > 0 && status != RUNNING)
        {
            timer->Schedule(start_compare);
            status = PENDING;
            has_pending = false;
        }
        else
        {
            if (start_compare <= 0)
            {
                start_compare += (int32_t)getLastTriggerDelta() * teeth_per_event;
            }
            if (start_compare > 0)
            {
                next_compare = start_compare + timer->Counter();
                has_pending = true;
            }
        }
    }
}

void scheduleInit()
{
    injSchedule1.timer->InitOutput(PIN_INJ1_lvl);
    injSchedule2.timer->InitOutput(PIN_INJ2_lvl);
    ignSchedule1.timer->InitOutput(PIN_IGN1_lvl);
    ignSchedule2.timer->InitOutput(PIN_IGN2_lvl);
}

void Schedule::InterruptHandler(bool (*allow)())
{
    tick_t counter = timer->Counter();
    if (status == Schedule::PENDING)
    {
        if (allow())
        {
            timer->Set(counter + duration);
            timer->Toggle();
            status = Schedule::RUNNING;
        }
        else
        {
            status = Schedule::OFF;
        }
        actual_period = TICKS_TO_US(counter - time_begin);
        time_begin = counter;
    }
    else if (status == Schedule::RUNNING)
    {
        actual_pw = TICKS_TO_US(counter - time_begin);
        if (has_pending)
        {
            timer->Set(next_compare);
            status = Schedule::PENDING;
            has_pending = false;
        }
        else
        {
            status = Schedule::OFF;
        }
    }
    else
    {
        status = Schedule::OFF;
    }
}

// limiter mechanics : https://www.motec.com.au/forum/viewtopic.php?f=11&t=175
static bool allow_inj()
{
    static uint8_t static_counter = 0;

    static_counter += tuning_settings.fuel.limiter_seed;
    return ((current_status.limiter_level_inj == 0) || (static_counter > current_status.limiter_level_inj));
}

static bool allow_ign()
{
    static uint8_t static_counter = 0;

    static_counter += tuning_settings.fuel.limiter_seed;
    return ((current_status.limiter_level_ign == 0) || (static_counter > current_status.limiter_level_ign));
}

TIMER10_CO_HANDLER()
{
    injSchedule1.InterruptHandler(allow_inj);
}

TIMER11_CO_HANDLER()
{
    injSchedule2.InterruptHandler(allow_inj);
}

TIMER7_CO_HANDLER()
{
    ignSchedule1.InterruptHandler(allow_ign);
}

TIMER6_CO_HANDLER()
{
    ignSchedule2.InterruptHandler(allow_ign);
}
