#include <stdint.h>

#include "global_objects.h"
#include "hardware.h"
#include "sensors.h"
#include "storage.h"
#include "table.h"

static uint16_t map_min_adc = 0x0fff, map_max_adc = 0x0000;

static uint16_t interpolate(uint16_t adc, uint16_t* axis, uint8_t* values, uint8_t multiplier)
{
    uint16_t result;
    uint16_t lmul = multiplier;
    if (adc <= axis[0])
    {
        result = lmul * values[0];
    }
    else if (adc >= axis[1])
    {
        result = lmul * values[1];
    }
    else
    {
        uint16_t a = lmul * values[0];
        uint32_t b = adc - axis[0];
        uint16_t c = lmul * (values[1] - values[0]);
        uint16_t d = axis[1] - axis[0];

        result = a + (b * c) / d;
    }
    return result;
}

void updateMapSensor(uint16_t adc_reading)
{
    current_status.mapADC = adc_reading;
    if (current_status.status1.run)
    {
        if (adc_reading < map_min_adc)
        {
            map_min_adc = adc_reading;
        }
        if (adc_reading > map_max_adc)
        {
            map_max_adc = adc_reading;
        }
    }
    else
    {
        map_min_adc = map_max_adc = adc_reading;
    }
}

void updateTpsSensor(uint16_t adc_reading)
{
    current_status.tpsADC = adc_reading;
    int32_t new_value = interpolate(adc_reading, tuning_settings.calib.tps_axis, tuning_settings.calib.tps_values, 1);
    current_status.tps += (new_value - current_status.tps) * (tuning_settings.fuel.analog_lag.tps) / 8;
}

void updateEgoSensor(uint16_t adc_reading)
{
    current_status.egoADC = adc_reading;
    int32_t new_value = interpolate(adc_reading, tuning_settings.calib.ego_axis, tuning_settings.calib.ego_values, 8);
    current_status.ego += (new_value - current_status.ego) * (tuning_settings.fuel.analog_lag.ego) / 8;
}

void updateBrvSensor(uint16_t adc_reading)
{
    current_status.brvADC = adc_reading;
    int32_t new_value = interpolate(adc_reading, tuning_settings.calib.brv_axis, tuning_settings.calib.brv_values, 2);
    current_status.brv += (new_value - current_status.brv) * (tuning_settings.fuel.analog_lag.brv) / 8;
    axis_brv.Update(current_status.brv);
}

void updateCltSensor(uint16_t adc_reading)
{
    current_status.cltADC = adc_reading;
    int32_t new_value = calib_clt.getValue(adc_reading);
    current_status.clt += (new_value - current_status.clt) * (tuning_settings.fuel.analog_lag.clt) / 8;
    axis_clt.Update(current_status.clt);

    current_status.idle_target = table_idle.getValue();
}

void updateMatSensor(uint16_t adc_reading)
{
    current_status.matADC = adc_reading;
    int32_t new_value = calib_mat.getValue(adc_reading);
    current_status.mat += (new_value - current_status.mat) * (tuning_settings.fuel.analog_lag.mat) / 8;
    axis_mat.Update(current_status.mat);
}

static void (*updateSensor[])(uint16_t) = {
    updateMapSensor,
    updateTpsSensor,
    updateEgoSensor,
    updateBrvSensor,
    updateCltSensor,
    updateMatSensor,
};

void sensorsInit()
{
    calib_clt.min = tuning_settings.calib.clt_min;
    calib_clt.max = tuning_settings.calib.clt_max;
    calib_clt.def_value = tuning_settings.calib.clt_default;

    calib_mat.min = tuning_settings.calib.mat_min;
    calib_mat.max = tuning_settings.calib.mat_max;
    calib_mat.def_value = tuning_settings.calib.mat_default;

    bool success = true;
    success &= analogAttachPin(tuning_settings.fuel.analog_input.brv, (uint8_t)SensorIndex::BRV);
    success &= analogAttachPin(tuning_settings.fuel.analog_input.map, (uint8_t)SensorIndex::MAP);
    success &= analogAttachPin(tuning_settings.fuel.analog_input.mat, (uint8_t)SensorIndex::MAT);
    success &= analogAttachPin(tuning_settings.fuel.analog_input.clt, (uint8_t)SensorIndex::CLT);
    success &= analogAttachPin(tuning_settings.fuel.analog_input.tps, (uint8_t)SensorIndex::TPS);
    success &= analogAttachPin(tuning_settings.fuel.analog_input.ego, (uint8_t)SensorIndex::EGO);

    current_status.debug = (!success) * 0x55;
}

void updateSensors(SensorIndex index, uint16_t adc_reading)
{
    updateSensor[(uint8_t)index](adc_reading);
}

void updateMapValue()
{
    uint16_t temp_adc;
    if (current_status.status1.run)
    {
        temp_adc = (map_min_adc + map_max_adc) / 2;
        map_min_adc = temp_adc;
        map_max_adc = temp_adc;
    }
    else
    {
        temp_adc = map_min_adc;
    }
    int32_t new_value = interpolate(temp_adc, tuning_settings.calib.map_axis, tuning_settings.calib.map_values, 20);
    ;
    current_status.map += (new_value - current_status.map) * tuning_settings.fuel.analog_lag.map / 8;

    current_status.engineLoad = current_status.map;
    axis_load.Update(current_status.engineLoad);
    axis_limiter_load.Update(current_status.engineLoad);
    axis_launch_load.Update(current_status.engineLoad);
}