
#include "state_machine.h"

#include "global_objects.h"
#include "storage.h"
#include "tasks.h"

inline void CrankingBegin()
{
    current_status.engine.crank = true;
    current_status.cranking_pw = 0;

    current_status.n_squirts = 1;
    current_status.teeth_per_event = 4 / 2;
}
inline void CrankingEnd()
{
    current_status.engine.crank = false;
    current_status.engine.warmup = true;

    current_status.n_squirts = (tuning_settings.fuel.n_squirts > 0) ? tuning_settings.fuel.n_squirts : 1;
    current_status.teeth_per_event = 4 / current_status.n_squirts;

    ase.init();
}
inline void RunningBegin()
{
    current_status.status1.run = true;
}
inline void EngineStopped()
{
    current_status.status1.run = false;
    current_status.engine.crank = false;
    current_status.enrich_ase = 0;
    current_status.engine.ase = false;
}

inline void updateLimiterStatus()
{
    const bool was_active = current_status.status1.sft_limit || current_status.status1.hrd_limit;

    current_status.status1.sft_limit = current_status.rpm >= tuning_settings.spark.limiter_rpm[0];
    current_status.status1.hrd_limit = current_status.rpm >= tuning_settings.spark.limiter_rpm[3];

    const bool is_active = current_status.status1.sft_limit || current_status.status1.hrd_limit;

    if (was_active || is_active)
    {
        axis_limiter_rpm.Update(current_status.rpm);

        if (current_status.status1.hrd_limit && tuning_settings.spark.hard_limit_fuel)
        {
            current_status.limiter_level_inj = 0xff;
        }
        else if (current_status.status1.sft_limit && tuning_settings.spark.soft_limit_fuel)
        {
            current_status.limiter_level_inj = table_limiter.getValue();
        }
        else
        {
            current_status.limiter_level_inj = 0;
        }

        if (current_status.status1.hrd_limit && tuning_settings.spark.hard_limit_spark)
        {
            current_status.limiter_level_ign = 0xff;
        }
        else if (current_status.status1.sft_limit && tuning_settings.spark.soft_limit_spark)
        {
            current_status.limiter_level_ign = table_limiter.getValue();
        }
        else
        {
            current_status.limiter_level_ign = 0;
        }
    }
}

void updateEngineState()
{
    if (!current_status.engine.ready || current_status.rpm == 0)
    {
        EngineStopped();
    }
    else if (current_status.status1.run)
    {
        if (current_status.engine.crank)
        {
            if (current_status.rpm > (20 * tuning_settings.spark.cranking_rpm))
            {
                CrankingEnd();
            }
        }
        else
        {
            updateLimiterStatus();

            bool launch_trigger = current_status.status2.launch_arm && (current_status.rpm >= tuning_settings.spark.launch_rpm[0]);
            if (current_status.status2.launch_active)
            {
                if (launch_trigger)
                {
                    axis_launch_rpm.Update(current_status.rpm);
                    current_status.limiter_level_ign = table_launch.getValue();
                }
                else
                {
                    current_status.limiter_level_ign = 0;
                    current_status.status2.launch_active = false;
                }
            }
            else
            {
                if (launch_trigger)
                {
                    current_status.status2.launch_active = true;
                }
            }
        }
    }
    else
    {
        RunningBegin();
        if (current_status.rpm <= (20 * tuning_settings.spark.cranking_rpm))
        {
            CrankingBegin();
        }
    }
}
