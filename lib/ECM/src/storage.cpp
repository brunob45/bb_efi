#include <Arduino.h>
#include <EEPROM.h>
#include <assert.h>

#include "storage.h"

#define ARRAY(x) ((uint8_t*)&x)

CurrentStatus current_status;
TuningSettings tuning_settings;

uint8_t* const page_arrays[] = {ARRAY(tuning_settings.fuel), ARRAY(tuning_settings.fuel), ARRAY(tuning_settings.tables), ARRAY(tuning_settings.spark), ARRAY(tuning_settings.tables2), ARRAY(tuning_settings.calib)};
const uint16_t page_sizes[] = {0, sizeof(PageFuelStruct), sizeof(PageTablesStruct), sizeof(PageSparkStruct), sizeof(PageTables2Struct), sizeof(PageCalibStruct)};
const uint16_t page_offsets[] = {0, 0, 256, 832, 1088, 4096 - sizeof(PageCalibStruct)};

static StorageBurnInfoStruct StorageBurnInfo = {0, ARRAY(tuning_settings.fuel), 0, 0, 0};

uint8_t* page2ByteArray(uint8_t n)
{
    return page_arrays[n];
}

bool initWriteStorage(uint8_t page_index)
{
    uint8_t n = page_index;
    StorageBurnInfo.page_index = n;
    StorageBurnInfo.current_page = page2ByteArray(n);
    StorageBurnInfo.offset = page_offsets[n];
    StorageBurnInfo.size = page_sizes[n];
    StorageBurnInfo.index = 0;
    return true;
}

uint8_t doWriteStorage()
{
    EEPROM.update(StorageBurnInfo.offset + StorageBurnInfo.index, StorageBurnInfo.current_page[StorageBurnInfo.index]);
    StorageBurnInfo.index++;

    return (StorageBurnInfo.index >= StorageBurnInfo.size) ? (1 << StorageBurnInfo.page_index) : 0;
}

void readStorage()
{
    for (uint8_t n = 1; n <= 5; n++)
    {
        uint8_t* byte_array = page2ByteArray(n);
        for (uint16_t i = 0; i < page_sizes[n]; i++)
        {
            byte_array[i] = EEPROM.read(page_offsets[n] + i);
        }
    }
}

void writeAt(uint16_t offset, uint16_t value)
{
    EEPROM.update(offset, value & UINT8_MAX);
    EEPROM.update(offset + 1, (value >> 8) & UINT8_MAX);
}

void writeCalib(uint8_t n, uint8_t index, int16_t value)
{
    uint16_t offset = 3964 + (66 * n) + (2 * index);
    EEPROM.update(offset, value & UINT8_MAX);
    EEPROM.update(offset + 1, (value >> 8) & UINT8_MAX);
}

#if defined(ARDUINO)
static_assert(sizeof(PageFuelStruct) == 145, "Size of Page Fuel changed");
static_assert(sizeof(PageSparkStruct) == 98, "Size of Page Spark changed");
static_assert(sizeof(PageCalibStruct) == 168, "Size of Page Calib changed");
static_assert(sizeof(PageTablesStruct) == 576, "Size of Page Tables changed");
static_assert(sizeof(PageTables2Struct) == 256, "Size of Page Tables2 changed");
#endif
