#include "table.h"
#include "storage.h"

struct TableIndexResult
{
    uint8_t index;
    bool is_exact;
};

void TableAxis::Update(int16_t x)
{
    if (x >= bins[size - 1])
    {
        bin = size - 1;
        w = 0;
    }
    else if (x >= bins[bin] && x < bins[bin + 1])
    {
        if (x > bins[bin])
        {
            w = int32_t(x - bins[bin]) * 256 / (bins[bin + 1] - bins[bin]);
        }
        else if (x == bins[bin])
        {
            w = 0;
        }
    }
    else
    {
        for (uint8_t i = size - 1; i > 0; i--)
        {
            uint8_t index = i - 1;
            if (x > bins[index])
            {
                bin = index;
                w = int32_t(x - bins[index]) * 256 / (bins[i] - bins[index]);
                break;
            }
            else if (x == bins[index])
            {
                bin = index;
                w = 0;
                break;
            }
        }
    }
}

int16_t Table1d::getValue(uint16_t x)
{
    if ((x < min) || (x >= max)) return def_value;
    x -= min;
    uint8_t index = CALIB_INDEX(x);
    int16_t result = values[index];

    uint8_t x1 = x & (CALIB_SIZE - 1);
    if (x1 != 0)
    {
        result += CALIB_INDEX(x1 * (values[index + 1] - result));
    }

    return result;
}

static inline int32_t Interpolate(uint16_t a, uint16_t b, uint8_t w)
{
    return a + ((int32_t)b - a) * w / 256;
}

uint16_t Table2d::getValue()
{
    uint16_t result = 0;

    if (axis->w == 0)
    {
        result = multiplier * values[axis->bin];
    }
    else
    {
        result = Interpolate(multiplier * values[axis->bin], multiplier * values[axis->bin + 1], axis->w);
    }

    return result;
}

uint16_t Table3d::getValue()
{
    uint16_t result = 0;
    uint8_t size = axis_x->size;

    uint8_t z = axis_x->bin + axis_y->bin * size;
    if (axis_x->w == 0)
    {
        if (axis_y->w == 0)
        {
            result = multiplier * values[z];
        }
        else
        {
            result = Interpolate(multiplier * values[z], multiplier * values[z + size], axis_y->w);
        }
    }
    else
    {
        int16_t result1 = Interpolate(multiplier * values[z], multiplier * values[z + 1], axis_x->w);

        if (axis_y->w == 0)
        {
            result = result1;
        }
        else
        {
            int16_t result2 = Interpolate(multiplier * values[z + size], multiplier * values[z + size + 1], axis_x->w);

            result = Interpolate(result1, result2, axis_y->w);
        }
    }

    return result;
}
