#include "tasks.h"

#include "auxPWM.h"
#include "calcs.h"
#include "comms.h"
#include "decoder.h"
#include "global_objects.h"
#include "schedule.h"
#include "sensors.h"
#include "state_machine.h"

static void updateAccelEnrich();
static void updateFuelPump();
static void updateFan();

static bool task_updateSchedules(Context&);
static bool task_updateMapSensor(Context&);
static bool task_updateSensors(Context&);
static bool task_updateAE_10ms(Context&);
static bool task_updateMapDot_10ms(Context&);
static bool task_updateASE_60ms(Context&);
static bool task_updateControl_100ms(Context&);
static bool task_mainLoop(Context&);

const Task tasks[] = {
    task_updateSchedules,
    task_updateMapSensor,
    task_updateSensors,
    task_updateMapDot_10ms,
    task_updateAE_10ms,
    task_updateASE_60ms,
    task_updateControl_100ms,
    task_mainLoop, // Main loop always last
};

const uint8_t NUM_TASKS = sizeof_array(tasks);

static bool task_updateSchedules(Context&)
{
    if (DecoderBuffer.empty())
    {
        return false;
    }

    if (updateDecoderInfo(DecoderBuffer.get()))
    {
        if (current_status.pw > 0 && tuning_settings.fuel.fuel_enable)
        {
            current_status.inj1ActualPw = injSchedule1.actual_pw;
            current_status.inj2ActualPw = injSchedule2.actual_pw;
            current_status.inj1Period = injSchedule1.actual_period;
            current_status.inj2Period = injSchedule2.actual_period;

            injSchedule1.SetFromAngle(tuning_settings.fuel.inj1_angle, current_status.pw, current_status.teeth_per_event);
            injSchedule2.SetFromAngle(tuning_settings.fuel.inj2_angle, current_status.pw, current_status.teeth_per_event);
        }
        else
        {
            current_status.inj1ActualPw = 0;
            current_status.inj2ActualPw = 0;
        }

        if (current_status.dwell > 0 && tuning_settings.fuel.spark_enable)
        {
            current_status.ign1ActualPw = ignSchedule1.actual_pw;
            current_status.ign2ActualPw = ignSchedule2.actual_pw;
            current_status.ign1Period = ignSchedule1.actual_period;
            current_status.ign2Period = ignSchedule2.actual_period;

            ignSchedule1.SetFromAngle(3600 - current_status.advance, current_status.dwell, 2);
            ignSchedule2.SetFromAngle(1800 - current_status.advance, current_status.dwell, 2);
        }
        else
        {
            current_status.ign1ActualPw = 0;
            current_status.ign2ActualPw = 0;
        }

        current_status.rpm = getRpm();
        axis_rpm.Update(current_status.rpm);
    }

    return true;
}

static bool task_updateASE_60ms(Context& context)
{
    if (!current_status.engine.ase || (context.current_millis & 0xff) - ase.last_update < ase.taper)
    {
        return false;
    }
    ase.last_update = context.current_millis;

    ase.update();

    return true;
}

static bool task_updateSensors(Context&)
{
    AtomicBlock atomic;
    if (AnalogBuffer.empty())
    {
        atomic.release();
        return false;
    }

    AnalogReading reading = AnalogBuffer.get();
    atomic.release();

    updateSensors((SensorIndex)reading.index, reading.value);

    return true;
}

static bool task_updateAE_10ms(Context& context)
{
    static uint8_t last_update = 0;

    if (current_status.accel_timeout == 0 || (context.current_millis & 0xff) - last_update < 10)
    {
        return false;
    }
    last_update = context.current_millis;

    current_status.accel_timeout--;
    if (current_status.accel_timeout == 0)
    {
        current_status.accel_enrich = 0;
        current_status.engine.mapaen = false;
    }

    return true;
}

static bool task_updateMapSensor(Context& ctx)
{
    static uint8_t last_update = 0;

    if (current_status.status1.run)
    {
        if (!updateMAP.GetAndReset())
        {
            return false;
        }
    }
    else if ((ctx.current_millis & 0xff) - last_update < 50)
    {
        return false;
    }

    last_update = ctx.current_millis;

    updateMapValue();

    return true;
}

static void updateAccelEnrich()
{
    if ((current_status.mapdot >= tuning_settings.fuel.ae_th) && (current_status.tps >= tuning_settings.fuel.ae_tps))
    {
        axis_accel_mapdot.Update(current_status.mapdot);
        axis_accel_rpm.Update(current_status.rpm);
        uint32_t accel = table_accel_mapdot.getValue();
        accel = (accel * table_accel_rpm.getValue()) / 128;

        if (accel > current_status.accel_enrich)
        {
            current_status.accel_enrich = accel;
            current_status.accel_timeout = tuning_settings.fuel.ae_dur;
            current_status.engine.mapaen = true;
        }
    }
}

static bool task_updateMapDot_10ms(Context& context)
{
    static uint16_t last_map = 0;
    static uint16_t last_mapdot_update = 0;

    if ((context.current_millis - last_mapdot_update) < 10)
    {
        return false;
    }

    if (!current_status.status1.run)
    {
        current_status.mapdot = 0;
        return false;
    }

    current_status.mapdot = ((int32_t)current_status.map - last_map) * 100 / (context.current_millis - last_mapdot_update);
    last_map = current_status.map;
    last_mapdot_update = context.current_millis;

    updateAccelEnrich();

    return true;
}

static void updateFuelPump()
{
    static uint8_t fp_time = 0;

    if (current_status.status2.fp_on)
    {
        if (!current_status.status2.fp_primed)
        {
            if (--fp_time == 0)
            {
                current_status.status2.fp_primed = true;
            }
        }
        else if (!current_status.status1.run)
        {
            io_fuel_pump.setLow();
            current_status.status2.fp_on = false;
        }
    }
    else
    {
        if (current_status.brv >= BRV_VALUE(8.0))
        {
            if (!current_status.status2.fp_primed)
            {
                if (tuning_settings.fuel.fp_prime_time > 0)
                {
                    io_fuel_pump.setHigh();
                    fp_time = tuning_settings.fuel.fp_prime_time;
                    current_status.status2.fp_on = true;
                }
                else
                {
                    current_status.status2.fp_primed = true;
                }
            }
            if (current_status.status1.run)
            {
                io_fuel_pump.setHigh();
                current_status.status2.fp_on = true;
            }
        }
    }
}

static void updateFan()
{
    if (current_status.status1.fan)
    {
        if (current_status.engine.crank || (current_status.clt < 10 * tuning_settings.fuel.fan_off_temp))
        {
            io_fan.setLow();
            current_status.status1.fan = false;
        }
    }
    else
    {
        if (current_status.clt >= 10 * tuning_settings.fuel.fan_on_temp)
        {
            io_fan.setHigh();
            current_status.status1.fan = true;
        }
    }
}

static bool task_updateControl_100ms(Context& ctx)
{
    static uint8_t last_update = 0;

    if ((ctx.current_millis & 0xff) - last_update < 100)
    {
        return false;
    }
    last_update = ctx.current_millis;

    if (!current_status.engine.crank)
    {
        if ((current_status.n_squirts != tuning_settings.fuel.n_squirts) && (tuning_settings.fuel.n_squirts > 0))
        {
            current_status.n_squirts = tuning_settings.fuel.n_squirts;
            current_status.teeth_per_event = 4 / current_status.n_squirts;
        }
    }

    updateFuelPump();
    updateFan();
    setDuty(tuning_settings.fuel.idle_duty);

    if (current_status.engine.crank)
    {
        if (current_status.cranking_pw < tuning_settings.fuel.crank_pw_max)
        {
            current_status.cranking_pw += tuning_settings.fuel.crank_pw_add;
            if (current_status.cranking_pw > tuning_settings.fuel.crank_pw_max)
            {
                current_status.cranking_pw = tuning_settings.fuel.crank_pw_max;
            }
        }
    }

    return true;
}

static bool task_mainLoop(Context&)
{
    static uint8_t step_index = 1;

    switch (step_index)
    {
    case 1:
        updateEngineState();
        break;
    case 2:
        doCalcs();
        break;
    case 3:
        checkComms();
        step_index = 0;
        break;
    default:
        step_index = 0;
        break;
    }
    step_index++;
    return true;
}
