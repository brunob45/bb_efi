#include "vss.h"
#include "global_objects.h"
#include "hardware.h"

static tick_t last_trigger = 0;
static bool is_valid = false;
static bool trigger_updated = false;

void vssInit()
{
    TimerCapture3Enable();
}

void Timer3CaptureCallback(tick_t trigger_time)
{
    if (is_valid)
    {
        uint16_t newv = trigger_time - last_trigger;

        if (newv > current_status.vss_delta / 2)
        {
            int32_t dot = (int32_t)newv - current_status.vss_delta;
            current_status.vss_delta += (dot * tuning_settings.fuel.vss_lag_factor) / 256;
            last_trigger = trigger_time;
            trigger_updated = true;
        }
    }
    else
    {
        is_valid = true;
        last_trigger = trigger_time;
        trigger_updated = true;
    }
}

TIMER3_OVF_HANDLER()
{
    if (!trigger_updated)
    {
        is_valid = false;
        current_status.vss_delta = 0;
    }
    trigger_updated = false;
}