
#if !defined(ANALOG_H)
#define ANALOG_H

#include "buffer.h"

struct AnalogReading
{
    uint16_t index : 6;
    uint16_t value : 10;
};

extern Buffer<AnalogReading, 8> AnalogBuffer;

void analogInit();
bool analogAttachPin(uint8_t pin, uint8_t index);

#endif // ANALOG_H
