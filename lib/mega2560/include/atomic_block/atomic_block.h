#if !defined(ATOMIC_BLOCK_H)
#define ATOMIC_BLOCK_H

#include <avr/interrupt.h>

class AtomicBlock
{
private:
    const uint8_t mySREG;
    bool captured;

public:
    AtomicBlock(bool capt = true) : mySREG(SREG), captured(capt)
    {
        if (captured) { capture(); }
    }
    ~AtomicBlock()
    {
        if (captured) { release(); }
    }

    inline void capture()
    {
        cli();
        captured = true;
        // if (mySREG & (1 << SREG_I)) { io_atomic.setHigh(); }
    }
    inline void release()
    {
        // if (mySREG & (1 << SREG_I)) { io_atomic.setLow(); }
        captured = false;
        SREG = mySREG;
    }
};

#endif // ATOMIC_BLOCK_H