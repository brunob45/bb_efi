#if !defined(MEGA2560_H)
#define MEGA2560_H

#include <stdint.h>

#include "io/io.h"

#include "analog/analog.h"
#include "atomic_block/atomic_block.h"
#include "timer/timer.h"

void hardwareInit();

inline uint16_t short_millis()
{
    extern volatile unsigned long timer0_millis; // from wiring.c

    AtomicBlock atomic;
    uint16_t m = timer0_millis;
    atomic.release();

    return m;
}

inline uint16_t available_ram()
{
    extern uint8_t _end;
    return SP - (uint16_t)(&_end);
}

#endif // MEGA2560_H
