#if !defined(PINDEF_H)
#define PINDEF_H

#define PIN_ACTIVE_HIGH 0
#define PIN_ACTIVE_LOW 1

#define TRIG_GOING_HIGH 0
#define TRIG_GOING_LOW 1

//----------------------------------------------------------------------------
// Outputs
//----------------------------------------------------------------------------

#define PIN_LED_num 33
#define PIN_LED_lvl PIN_ACTIVE_HIGH
#define PIN_ATOMIC_num 34
#define PIN_ATOMIC_lvl PIN_ACTIVE_HIGH
#define PIN_TEST_num 35
#define PIN_TEST_lvl PIN_ACTIVE_HIGH

#define PIN_INJ1_num 45             // TIM5B
#define PIN_INJ1_lvl PIN_ACTIVE_LOW //
#define PIN_INJ2_num 44             // TIM5A, also 43
#define PIN_INJ2_lvl PIN_ACTIVE_LOW //

#define PIN_IGN1_num 7              // TIM4B, also 4
#define PIN_IGN1_lvl PIN_ACTIVE_LOW //
#define PIN_IGN2_num 6              // TIM4A
#define PIN_IGN2_lvl PIN_ACTIVE_LOW //

#define PIN_FAN_num 24
#define PIN_FAN_lvl PIN_ACTIVE_HIGH
#define PIN_ECL_num 8
#define PIN_ECL_lvl PIN_ACTIVE_LOW
#define PIN_FUEL_PUMP_num 61              // PIN_A7
#define PIN_FUEL_PUMP_lvl PIN_ACTIVE_HIGH //
#define PIN_IDLE_num 59                   // PIN_A5
#define PIN_IDLE_lvl PIN_ACTIVE_HIGH      //

//----------------------------------------------------------------------------
// Inputs
//----------------------------------------------------------------------------

#define PIN_TRIG1_num 18 // also 49
#define PIN_TRIG1_lvl TRIG_GOING_LOW
#define PIN_TRIG2_num 20
#define PIN_TRIG2_lvl TRIG_GOING_LOW

#define PIN_VSS_num 48
#define PIN_VSS_lvl TRIG_GOING_HIGH

#define PIN_CLUTCH_num 22
#define PIN_CLUTCH_lvl PIN_ACTIVE_LOW

#endif // PINDEF_H