#if !defined(TIMER_H)
#define TIMER_H

#include <stdint.h>

#include "timer_ll.h"

typedef uint16_t tick_t;

#define COUNTER_MAX ((tick_t)-1)
#define TIME (TCNT4)

#ifdef FAST_TIMER
#define TICKS_TO_US(ticks) ((ticks) / 2U)
#define US_TO_TICKS(us) ((us)*2U)
#else
#define TICKS_TO_US(ticks) ((ticks)*4U)
#define US_TO_TICKS(us) ((us) / 4U)
#endif

struct TimerAbstractStruct
{
    virtual void Init() = 0;
    virtual void InitOutput(bool invert) = 0;
    virtual void Set(tick_t ticks) = 0;
    virtual tick_t Counter() = 0;
    virtual void Toggle() = 0;
    inline void Schedule(tick_t ticks) { Set(Counter() + ticks); }
};
//----------------------------------------------------------------------------
template <int N>
struct TimerImplStruct : public TimerAbstractStruct, public TimerLL<N>
{
    typedef TimerLL<N> myTimerLL;

    void Init() final
    {
        myTimerLL::ClearInterrupt();
        myTimerLL::EnableInterrupt();
    }
    void InitOutput(bool invert) final
    {
        if (invert)
        {
            myTimerLL::SetOutput();
        }
        else
        {
            myTimerLL::ResetOutput();
        }

        myTimerLL::EnableOutput();
        myTimerLL::ForceOutput();

        Init();
    }
    void Set(tick_t ticks) final
    {
        myTimerLL::SetCompare(ticks);
        Clear();
    }
    tick_t Counter() final { return myTimerLL::GetCounter(); }
    void Toggle() final
    {
        myTimerLL::ToggleOutput();
        myTimerLL::ForceOutput();
        myTimerLL::ToggleOutput();
    }
    inline void Schedule(tick_t ticks) { Set(Counter() + ticks); }
    static TimerImplStruct<N>* Instance()
    {
        static TimerImplStruct<N> instance;
        return &instance;
    }

private:
    void Clear() { myTimerLL::ClearInterrupt(); }
};

void timersInit();

inline void TimerCapture0Enable() // PD4 - RX LED
{
    TCCR1B = TCCR1B | (1 << ICNC1);
    TIMSK1 = TIMSK1 | (1 << ICIE1);
}
inline void TimerCapture1Enable() // PE7 - NC
{
    TCCR3B = TCCR3B | (1 << ICNC3);
    TIMSK3 = TIMSK3 | (1 << ICIE3);
}
inline void TimerCapture2Enable() // PL0 - D49
{
    TCCR4B = TCCR4B | (1 << ICNC4);
    TIMSK4 = TIMSK4 | (1 << ICIE4);
}
inline void TimerCapture3Enable() // PL1 - D48
{
    TCCR5B = TCCR5B | (1 << ICNC5);
    TIMSK5 = TIMSK5 | (1 << ICIE5);
}

#endif // TIMER_H
