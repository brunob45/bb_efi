#if !defined(TIMER_LL_H)
#define TIMER_LL_H

#include <avr/io.h>

volatile uint8_t* const TCCRnA[] = {&TCCR1A, &TCCR1A, &TCCR1A, &TCCR3A, &TCCR3A, &TCCR3A, &TCCR4A, &TCCR4A, &TCCR4A, &TCCR5A, &TCCR5A, &TCCR5A};
volatile uint8_t* const TCCRnB[] = {&TCCR1B, &TCCR1B, &TCCR1B, &TCCR3B, &TCCR3B, &TCCR3B, &TCCR4B, &TCCR4B, &TCCR4B, &TCCR5B, &TCCR5B, &TCCR5B};
volatile uint8_t* const TCCRnC[] = {&TCCR1C, &TCCR1C, &TCCR1C, &TCCR3C, &TCCR3C, &TCCR3C, &TCCR4C, &TCCR4C, &TCCR4C, &TCCR5C, &TCCR5C, &TCCR5C};
volatile uint16_t* const TCNTn[] = {&TCNT1, &TCNT1, &TCNT1, &TCNT3, &TCNT3, &TCNT3, &TCNT4, &TCNT4, &TCNT4, &TCNT5, &TCNT5, &TCNT5};
volatile uint16_t* const OCRnX[] = {&OCR1A, &OCR1B, &OCR1C, &OCR3A, &OCR3B, &OCR3C, &OCR4A, &OCR4B, &OCR4C, &OCR5A, &OCR5B, &OCR5C};
volatile uint16_t* const ICRn[] = {&ICR1, &ICR1, &ICR1, &ICR3, &ICR3, &ICR3, &ICR4, &ICR4, &ICR4, &ICR5, &ICR5, &ICR5};
volatile uint8_t* const TIMSKn[] = {&TIMSK1, &TIMSK1, &TIMSK1, &TIMSK3, &TIMSK3, &TIMSK3, &TIMSK4, &TIMSK4, &TIMSK4, &TIMSK5, &TIMSK5, &TIMSK5};
volatile uint8_t* const TIFRn[] = {&TIFR1, &TIFR1, &TIFR1, &TIFR3, &TIFR3, &TIFR3, &TIFR4, &TIFR4, &TIFR4, &TIFR5, &TIFR5, &TIFR5};

const uint8_t COMnX[] = {COM1A0, COM1B0, COM1C0, COM3A0, COM3B0, COM3C0, COM4A0, COM4B0, COM4C0, COM5A0, COM5B0, COM5C0};
const uint8_t FOCnX[] = {FOC1A, FOC1B, FOC1C, FOC3A, FOC3B, FOC3C, FOC4A, FOC4B, FOC4C, FOC5A, FOC5B, FOC5C};
const uint8_t OCIEnX[] = {OCIE1A, OCIE1B, OCIE1C, OCIE3A, OCIE3B, OCIE3C, OCIE4A, OCIE4B, OCIE4C, OCIE5A, OCIE5B, OCIE5C};
const uint8_t OCFnX[] = {OCF1A, OCF1B, OCF1C, OCF3A, OCF3B, OCF3C, OCF4A, OCF4B, OCF4C, OCF5A, OCF5B, OCF5C};

constexpr uint8_t COMnXPin[] = {11, 12, 13, 5, 2, 3, 6, 7, 8, 46, 45, 44};

template <typename T, int N>
constexpr int sizeof_array(T (&)[N]) { return N; }

constexpr bool TimerLL_PinIsTimer(uint8_t pin)
{
    for (int i = 0; i < sizeof_array(COMnXPin); i++)
    {
        if (COMnXPin[i] == pin)
        {
            return true;
        }
    }
    return false;
}

constexpr uint8_t TimerLL_GetIndex(uint8_t pin)
{
    for (int i = 0; i < sizeof_array(COMnXPin); i++)
    {
        if (COMnXPin[i] == pin)
        {
            return i;
        }
    }
    return 0;
}

template <int Pin>
struct TimerLL
{
    static_assert(TimerLL_PinIsTimer(Pin), "Pin is not a valid timer output");
    constexpr static int N = TimerLL_GetIndex(Pin);

    inline void SetCompare(uint16_t ticks) { *OCRnX[N] = ticks; }
    inline uint16_t GetCompare() { return *OCRnX[N]; }

    inline uint16_t GetCounter() { return *TCNTn[N]; }

    inline void EnableInterrupt() { *TIMSKn[N] = *TIMSKn[N] | _BV(OCIEnX[N]); }
    inline void DisableInterrupt() { *TIMSKn[N] = *TIMSKn[N] & ~_BV(OCIEnX[N]); }
    inline void ClearInterrupt() { *TIFRn[N] = _BV(OCFnX[N]); }

    inline void EnableOutput() { *TCCRnA[N] = *TCCRnA[N] | (0b10 << COMnX[N]); }
    inline void DisableOutput() { *TCCRnA[N] = *TCCRnA[N] & ~(0b11 << COMnX[N]); }

    inline void SetOutput() { *TCCRnA[N] = *TCCRnA[N] | (0b01 << COMnX[N]); }
    inline void ResetOutput() { *TCCRnA[N] = *TCCRnA[N] & ~(0b01 << COMnX[N]); }
    inline void ToggleOutput() { *TCCRnA[N] = *TCCRnA[N] ^ (0b01 << COMnX[N]); }

    inline void ForceOutput() { *TCCRnC[N] = _BV(FOCnX[N]); }
};

#define TIMER0_CO_HANDLER(...) ISR(TIMER1_COMPA_vect, __VA_ARGS__)
#define TIMER1_CO_HANDLER(...) ISR(TIMER1_COMPB_vect, __VA_ARGS__)
#define TIMER2_CO_HANDLER(...) ISR(TIMER1_COMPC_vect, __VA_ARGS__)
#define TIMER0_OVF_HANDLER(...) ISR(TIMER1_OVF_vect, __VA_ARGS__)

#define TIMER3_CO_HANDLER(...) ISR(TIMER3_COMPA_vect, __VA_ARGS__)
#define TIMER4_CO_HANDLER(...) ISR(TIMER3_COMPB_vect, __VA_ARGS__)
#define TIMER5_CO_HANDLER(...) ISR(TIMER3_COMPC_vect, __VA_ARGS__)
#define TIMER1_OVF_HANDLER(...) ISR(TIMER3_OVF_vect, __VA_ARGS__)

#define TIMER6_CO_HANDLER(...) ISR(TIMER4_COMPA_vect, __VA_ARGS__)
#define TIMER7_CO_HANDLER(...) ISR(TIMER4_COMPB_vect, __VA_ARGS__)
#define TIMER8_CO_HANDLER(...) ISR(TIMER4_COMPC_vect, __VA_ARGS__)
#define TIMER2_OVF_HANDLER(...) ISR(TIMER4_OVF_vect, __VA_ARGS__)

#define TIMER9_CO_HANDLER(...) ISR(TIMER5_COMPA_vect, __VA_ARGS__)
#define TIMER10_CO_HANDLER(...) ISR(TIMER5_COMPB_vect, __VA_ARGS__)
#define TIMER11_CO_HANDLER(...) ISR(TIMER5_COMPC_vect, __VA_ARGS__)
#define TIMER3_OVF_HANDLER(...) ISR(TIMER5_OVF_vect, __VA_ARGS__)

#endif // TIMER_LL_H