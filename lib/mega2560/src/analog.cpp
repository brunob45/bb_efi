
#include <avr/interrupt.h>
#include <avr/io.h>

#include "analog/analog.h"

#define ADC_PRESCALER_2 ((0 << ADPS2) | (0 << ADPS1) | (1 << ADPS0))
#define ADC_PRESCALER_4 ((0 << ADPS2) | (1 << ADPS1) | (0 << ADPS0))
#define ADC_PRESCALER_8 ((0 << ADPS2) | (1 << ADPS1) | (1 << ADPS0))
#define ADC_PRESCALER_16 ((1 << ADPS2) | (0 << ADPS1) | (0 << ADPS0))
#define ADC_PRESCALER_32 ((1 << ADPS2) | (0 << ADPS1) | (1 << ADPS0))
#define ADC_PRESCALER_64 ((1 << ADPS2) | (1 << ADPS1) | (0 << ADPS0))
#define ADC_PRESCALER_128 ((1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0))

Buffer<AnalogReading, 8> AnalogBuffer;

static volatile uint8_t adc_array[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

static volatile uint8_t current_conversion_sgv = 0;

inline void selectInput(uint8_t input)
{
    current_conversion_sgv = input;
    ADMUX = (ADMUX & ~(7 << MUX0)) | ((input & 7) << MUX0);
    ADCSRB = (ADCSRB & ~(1 << MUX5)) | ((input > 7) << MUX5);
}

inline void startConversion()
{
    ADCSRA = ADCSRA | (1 << ADSC);
}

inline void selectNextInput(uint8_t current_input)
{
    for (uint8_t i = 1; i < 16; i++)
    {
        uint8_t candidate = (current_input + i) & 15;
        if (adc_array[candidate] != 0)
        {
            selectInput(candidate);
            break;
        }
    }
}

void analogInit()
{
    ADMUX = (0 << REFS1) | (1 << REFS0);
    ADCSRB = 0x00;
    ADCSRA = (1 << ADEN) | (1 << ADIF) | (1 << ADIE) | ADC_PRESCALER_128;
    selectNextInput(0);
    startConversion();
}

bool analogAttachPin(uint8_t pin, uint8_t index)
{
    bool success = (adc_array[pin] == 0);
    adc_array[pin] = (1 << 7) | index;
    return success;
}

ISR(ADC_vect)
{
    AnalogReading reading;

    reading.index = adc_array[current_conversion_sgv];
    reading.value = ADCW;

    if (AnalogBuffer.full())
    {
        DDRB = DDRB | (1 << 7);
        PORTB = PORTB | (1 << 7);
    }

    AnalogBuffer.put(reading);

    selectNextInput(current_conversion_sgv);
    startConversion();
}
