
#include <Arduino.h>

#include "io/io.h"

DigitalInput::DigitalInput(uint8_t pin, bool inverted)
    : DigitalIO(portInputRegister(digitalPinToPort(pin)), digitalPinToBitMask(pin), inverted)
{
    volatile uint8_t& portMode = *portModeRegister(digitalPinToPort(pin));
    portMode = portMode & ~_mask;
    setLow();
}

DigitalOutput::DigitalOutput(uint8_t pin, bool inverted)
    : DigitalIO(portOutputRegister(digitalPinToPort(pin)), digitalPinToBitMask(pin), inverted)
{
    volatile uint8_t& portMode = *portModeRegister(digitalPinToPort(pin));
    portMode = portMode | _mask;
    setLow();
}

DigitalOutput io_led(PIN_LED_num, PIN_LED_lvl);
DigitalOutput io_atomic(PIN_ATOMIC_num, PIN_ATOMIC_lvl);
DigitalOutput io_test(PIN_TEST_num, PIN_TEST_lvl);
