#include <avr/interrupt.h>

#include "analog/analog.h"
#include "hardware.h"

void hardwareInit()
{
    analogInit();
    timersInit();
}

// ISR(BADISR_vect, ISR_NAKED) { reti(); }