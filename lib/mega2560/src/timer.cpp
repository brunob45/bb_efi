
#include <avr/interrupt.h>
#include <avr/io.h>

#include "timer/timer.h"

#define TIMER_PRESCALER_OFF ((0 << CS12) | (0 << CS11) | (0 << CS10))
#define TIMER_PRESCALER_1 ((0 << CS12) | (0 << CS11) | (1 << CS10))
#define TIMER_PRESCALER_8 ((0 << CS12) | (1 << CS11) | (0 << CS10))
#define TIMER_PRESCALER_64 ((0 << CS12) | (1 << CS11) | (1 << CS10))
#define TIMER_PRESCALER_256 ((1 << CS12) | (0 << CS11) | (0 << CS10))
#define TIMER_PRESCALER_1024 ((1 << CS12) | (0 << CS11) | (1 << CS10))

#ifdef FAST_TIMER
#define TIMER_PRESCALER TIMER_PRESCALER_8
#else
#define TIMER_PRESCALER TIMER_PRESCALER_64
#endif

#define TIMER_MODE_NORMAL ((0 << WGM01) | (0 << WGM00))
#define TIMER_MODE_PWM ((0 << WGM01) | (1 << WGM00))
#define TIMER_MODE_CTC ((1 << WGM01) | (0 << WGM00))
#define TIMER_MODE_FASTPWM ((1 << WGM01) | (1 << WGM00))

#define ALL_TIF ((1 << ICF1) | (1 << OCF1C) | (1 << OCF1B) | (1 << OCF1A) | (1 << TOV1))

void timersInit()
{
    TCNT1 = 0;                  //Reset Timer Count
    TCCR1A = TIMER_MODE_NORMAL; //Timer1 Control Reg A: Wave Gen Mode normal
    TCCR1B = TIMER_PRESCALER;   //Timer1 Control Reg B: Timer Prescaler set to 64.
    TIFR1 = ALL_TIF;            //Clear the compare flags, overflow flag and external input flag bits

    TCNT3 = 0;                  //Reset Timer Count
    TCCR3A = TIMER_MODE_NORMAL; //Timer3 Control Reg A: Wave Gen Mode normal
    TCCR3B = TIMER_PRESCALER;   //Timer3 Control Reg B: Timer Prescaler set to 64.
    TIFR3 = ALL_TIF;            //Clear the compare flags, overflow flag and external input flag bits

    TCNT4 = 0;                  //Reset Timer Count
    TCCR4A = TIMER_MODE_NORMAL; //Timer4 Control Reg A: Wave Gen Mode normal
    TCCR4B = TIMER_PRESCALER;   //Timer4 Control Reg B: Timer Prescaler set to 64.
    TIFR4 = ALL_TIF;            //Clear the compare flags, overflow flag and external input flag bits

    TCNT5 = 0;                  //Reset Timer Count
    TCCR5A = TIMER_MODE_NORMAL; //Timer5 Control Reg A: Wave Gen Mode normal
    TCCR5B = TIMER_PRESCALER;   //Timer5 Control Reg B: Timer Prescaler set to 64.
    TIFR5 = ALL_TIF;            //Clear the compare flags, overflow flag and external input flag bits
}

void Timer0CaptureCallback(uint16_t) __attribute__((__weak__));
void Timer0CaptureCallback(uint16_t) {}

ISR(TIMER1_CAPT_vect)
{
    const uint16_t icr = ICR1;
    TCCR1B = TCCR1B ^ (1 << ICES1);
    TIFR1 = (1 << ICF1);

    Timer0CaptureCallback(icr);
}

void Timer1CaptureCallback(uint16_t) __attribute__((__weak__));
void Timer1CaptureCallback(uint16_t) {}

ISR(TIMER3_CAPT_vect)
{
    const uint16_t icr = ICR3;
    TCCR3B = TCCR3B ^ (1 << ICES3);
    TIFR3 = (1 << ICF3);

    Timer1CaptureCallback(icr);
}

void Timer2CaptureCallback(uint16_t) __attribute__((__weak__));
void Timer2CaptureCallback(uint16_t) {}

ISR(TIMER4_CAPT_vect)
{
    const uint16_t icr = ICR4;
    TCCR4B = TCCR4B ^ (1 << ICES4);
    TIFR4 = (1 << ICF4);

    Timer2CaptureCallback(icr);
}

void Timer3CaptureCallback(uint16_t) __attribute__((__weak__));
void Timer3CaptureCallback(uint16_t) {}

ISR(TIMER5_CAPT_vect)
{
    const uint16_t icr = ICR5;
    TCCR5B = TCCR5B ^ (1 << ICES5);
    TIFR5 = (1 << ICF5);

    Timer3CaptureCallback(icr);
}