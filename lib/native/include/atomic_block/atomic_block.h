#if !defined(ATOMIC_BLOCK_H)
#define ATOMIC_BLOCK_H

#include <avr/interrupt.h>

#include "hardware.h"

class AtomicBlockClass
{
private:
    const uint8_t mySREG;

public:
    AtomicBlockClass(bool capt = true) : mySREG(SREG)
    {
        if (capt) { capture(); }
    }

    inline void capture()
    {
        cli();
        // if (mySREG & (1 << SREG_I)) { io_atomic.setHigh(); }
    }
    inline void release()
    {
        // if (mySREG & (1 << SREG_I)) { io_atomic.setLow(); }
        SREG = mySREG;
    }
};

typedef AtomicBlockClass AtomicBlock;

#endif // ATOMIC_BLOCK_H