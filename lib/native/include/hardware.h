#if !defined(MEGA2560_H)
#define MEGA2560_H

#include <avr/interrupt.h>
#include <avr/io.h>

#include "analog/analog.h"
#include "atomic_block/atomic_block.h"
#include "io/io.h"
#include "timer/timer.h"

inline uint16_t short_millis()
{
    extern volatile unsigned long timer0_millis; // from wiring.c

    AtomicBlock atomic;
    uint16_t m = timer0_millis;
    atomic.release();

    return m;
}
inline void hardwareInit() {}
inline uint16_t available_ram() { return 0; }

#endif // MEGA2560_H
