#if !defined(IO_H)
#define IO_H

#include <stdint.h>

#include "pindef.h"

struct DigitalIO
{
    volatile uint8_t* _port;
    uint8_t _mask;
    bool _inverted;

    DigitalIO() : _port(0), _mask(0), _inverted(false) {}
    DigitalIO(volatile uint8_t* port, uint8_t mask, bool inverted) : _port(port), _mask(mask), _inverted(inverted) {}

    inline void set(uint8_t level)
    {
        if (_port)
        {
            (!level ^ !_inverted) ? * _port |= _mask : * _port &= ~_mask;
        }
    }
    inline void setHigh()
    {
        set(1);
    }
    inline void setLow()
    {
        set(0);
    }
    inline uint8_t get() { return _port ? !(*_port & _mask) ^ !_inverted : 0; }
};

struct DigitalInput : DigitalIO
{
    DigitalInput(uint8_t pin, bool inverted = false);
};

struct DigitalOutput : DigitalIO
{
    DigitalOutput(uint8_t pin, bool inverted = false);
};

extern DigitalOutput io_led;
extern DigitalOutput io_atomic;

#endif // IO_H