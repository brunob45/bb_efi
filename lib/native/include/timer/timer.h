#if !defined(TIMER_H)
#define TIMER_H

#include "hardware.h"

typedef uint16_t tick_t;

#define COUNTER_MAX ((tick_t)-1)
#define TIME (_time)

#define TICKS_TO_US(ticks) ((ticks) << 2)
#define US_TO_TICKS(us) (((us) >> 2) - 1)

#define TIMER0_CO_HANDLER(...) void timer0Handler()
#define TIMER1_CO_HANDLER(...) void timer1Handler()
#define TIMER2_CO_HANDLER(...) void timer2Handler()

#define TIMER3_CO_HANDLER(...) void timer3Handler()
#define TIMER4_CO_HANDLER(...) void timer4Handler()
#define TIMER5_CO_HANDLER(...) void timer5Handler()

#define TIMER6_CO_HANDLER(...) void timer6Handler()
#define TIMER7_CO_HANDLER(...) void timer7Handler()
#define TIMER8_CO_HANDLER(...) void timer8Handler()

#define TIMER9_CO_HANDLER(...) void timer9Handler()
#define TIMER10_CO_HANDLER(...) void timer10Handler()
#define TIMER11_CO_HANDLER(...) void timer11Handler()

template <typename T, int N>
constexpr int sizeof_array(T (&)[N])
{
    return N;
}

struct TimerAbstractStruct
{
    virtual void Init(bool) = 0;
    void Set(tick_t) { return; }
    tick_t Counter(void) { return 0; }
    void Clear(void) { return; }
    void Toggle(void) { return; }

    inline void Schedule(tick_t ticks) { Set(Counter() + ticks); }
};

template <int N>
struct TimerImplStruct : public TimerAbstractStruct
{
    void Init(bool) final {}
    static TimerImplStruct<N>* Instance()
    {
        static TimerImplStruct<N> instance;
        return &instance;
    }
};

extern tick_t _time;

void timersInit();

inline void TimerCapture0Enable() {}
inline void TimerCapture1Enable() {}
inline void TimerCapture2Enable() {}
inline void TimerCapture3Enable() {}

#endif // TIMER_H
