#include <stdint.h>

#include "analog/analog.h"

volatile uint16_t map_adc_reading = 0;
volatile uint16_t tps_adc_reading = 0;
volatile uint16_t clt_adc_reading = 0;
volatile uint16_t mat_adc_reading = 0;
volatile uint16_t ego_adc_reading = 0;
volatile uint16_t brv_adc_reading = 0;

void analogInit() {}
