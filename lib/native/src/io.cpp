#include "io/io.h"

DigitalInput::DigitalInput(uint8_t, bool inverted) : DigitalIO(0, 0, inverted) {}
DigitalOutput::DigitalOutput(uint8_t, bool inverted) : DigitalIO(0, 0, inverted) {}

DigitalOutput io_led(13);
DigitalOutput io_atomic(22);
