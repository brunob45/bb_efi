#include "pid.h"

PID::PID(uint8_t p, uint8_t i, uint8_t d, int16_t bias, int16_t min, int16_t max)
    : _p(p), _i(i), _d(d), _bias(bias), _min(min), _max(max), _integral(0), _previous_error(0), _init(false)
{
}

int32_t PID::Update(int32_t input, int32_t setpoint)
{
    int32_t error = setpoint - input;
    int32_t integral = _integral + (error / 16);
    int32_t derivative = (error - _previous_error) * 16;

    if (!_init)
    {
        derivative = 0;
        _init = true;
    }

    int32_t P = error * _p;
    int32_t I = integral * _i;
    int32_t D = derivative * _d;

    int16_t output = _bias + ((P + I + D) >> 3);

    if (output < _min)
    {
        output = _min;
    }
    else if (output > _max)
    {
        output = _max;
    }
    else
    {
        _integral = integral;
    }

    _previous_error = error;

    return output;
}
