#if !defined(PID_H)
#define PID_H

#include <stdint.h>

class PID
{
    uint8_t _p, _i, _d;
    int16_t _bias, _min, _max;
    int32_t _integral, _previous_error;
    bool _init;

public:
    PID(uint8_t p, uint8_t i, uint8_t d, int16_t bias = 0, int16_t min = 0x8000, int16_t max = 0x7fff);

    int32_t Update(int32_t input, int32_t setpoint);

    inline void P(uint8_t p) { _p = p; }
    inline void I(uint8_t i) { _i = i; }
    inline void D(uint8_t d) { _d = d; }
    inline void Reset()
    {
        _integral = 0;
        _init = false;
    }
    inline void Reset(uint8_t p, uint8_t i, uint8_t d)
    {
        P(p);
        I(i);
        D(d);
        Reset();
    }
};

#endif // PID_H