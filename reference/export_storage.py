#!/usr/bin/env python3

import re

scalar_index = 0


class Scalar:
    def __init__(self, items):
        global scalar_index
        self.index = scalar_index

        (self.title, self.type, self.size,
         self.units, self.scale, self.offset, self.logt, self.logd) = items

        # try:
        #     self.scale = str("%.3f" % float(self.scale))
        # except:
        #     pass

        # try:
        #     self.offset = str("%.3f" % float(self.offset))
        # except:
        #     pass

        self.logt = self.logt.replace("${title}", self.title)

        self.bits = []

        scalar_index += int(self.size[1:])//8
        # print(self.title, self.index)


class Bit:
    def __init__(self, items):
        global calcs

        (self.title, self.type, self.size,
         self.reference, self.index, self.null, self.logt, self.logd) = items

        if len(self.logt) > 0:
            self.logt = self.logt.replace("${title}", self.title)
            calc_title = "log_"+self.title
            calcs[calc_title] = Calc([calc_title,
                                      "calc", self.title + " ? 1 : 0", None, None, None, self.logt, "0"])


class Calc:
    def __init__(self, items):
        self.title = items[0]
        self.expression = items[2]
        self.logt = items[6]
        self.logt = self.logt.replace("${title}", self.title)
        self.logd = items[7]


def format_logtitle(item):
    if len(item.logd) > 0:
        logd = int(item.logd)
        if logd == 0:
            t = "int"
            d = "%d"
        else:
            t = "float"
            d = "%." + str(logd) + "f"
        return (item.title + ',').ljust(20) + '\"' + (item.logt + "\",").ljust(20) + (t + ',').ljust(8) + '\"' + d + '\"'


def print_OutputChannels(scalars, calcs):
    datalog_values = []
    template_in = open("template.ini", 'r')
    file_out = open("firmware.ini", 'w+')

    for line in template_in:
        file_out.write(line)

    file_out.write("[OutputChannels]\n")
    file_out.write("    ochGetCommand = \"r\\$tsCanId\\x30%2o%2c\"\n")
    file_out.write("    ochBlockSize = " + str(scalar_index)+'\n\n')

    for scalar in scalars.values():
        if len(scalar.bits) > 0:
            file_out.write('\n')
        file_out.write("    " + scalar.title.ljust(20) + '= ')
        file_out.write((scalar.type + ',').ljust(10))
        file_out.write((scalar.size + ',').ljust(8))
        file_out.write((str(scalar.index)+',').ljust(4))
        file_out.write(('"' + scalar.units + "\",").ljust(12))
        file_out.write((scalar.scale + ',').ljust(12))
        file_out.write(scalar.offset + '\n')
        log = format_logtitle(scalar)
        if log:
            datalog_values.append(log)

        if len(scalar.bits) > 0:
            for bit in scalar.bits:
                file_out.write("    " + bit.title.ljust(20) + '= ')
                file_out.write((bit.type + ',').ljust(10))
                file_out.write((bit.size + ', ').ljust(8))
                file_out.write(str(scalars[bit.reference].index) + ', ')
                file_out.write('[' + bit.index + ':' + bit.index + ']\n')
            file_out.write('\n')

    file_out.write('\n')
    for calc in calcs.values():
        file_out.write("    " + calc.title.ljust(20) + '= ')
        file_out.write("{ " + calc.expression + " }\n")
        log = format_logtitle(calc)
        if log:
            datalog_values.append(log)

    file_out.write("\n[Datalog]\n")
    file_out.write(
        "    ;       Channel             Label                Type    Format\n")
    file_out.write(
        "    ;       ------------------  -------------------  ------  ------\n")
    for value in datalog_values:
        file_out.write("    entry = " + value + '\n')


def print_currentStatus(scalars):
    file_out = open("../lib/ECM/include/current_status.h", "w+")
    file_out.write(
        "#if !defined(CURRENT_STATUS_H)\n#define CURRENT_STATUS_H\n\n")
    file_out.write("#include <stdint.h>\n\n")
    file_out.write("struct CurrentStatus {\n")

    for scalar in scalars.values():
        if len(scalar.bits) > 0:
            file_out.write("    struct {\n")
            for bit in scalar.bits:
                file_out.write("        bool " +
                               bit.title.replace("flag_", "") + " : 1;\n")
            file_out.write(
                "    } " + scalar.title.replace("flags_", "") + ";\n")
        else:
            file_out.write("    u" if 'U' in scalar.size else "    ")
            file_out.write("int8_t " if "08" in scalar.size else "int16_t ")
            file_out.write(scalar.title + ";\n")

    file_out.write("};\n\n")
    file_out.write("#endif // CURRENT_STATUS_H\n")


scalars = {}
calcs = {}


file_in = open("storage.csv", 'r')
for line in file_in:
    items = line.strip().split(',')
    if items[1] == "scalar":
        scalars[items[0]] = Scalar(items)
    elif items[1] == "bits":
        bit = Bit(items)
        scalars[bit.reference].bits.append(bit)
    elif items[1] == "calc":
        calcs[items[0]] = Calc(items)
    else:
        # print("Unknown line", line)
        pass
file_in.close()

print_OutputChannels(scalars, calcs)
print_currentStatus(scalars)
