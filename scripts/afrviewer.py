#!/usr/bin/env python3

import sys
import math

from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QAction, QTableWidget,QTableWidgetItem,QVBoxLayout
from PyQt5.QtGui import QIcon, QColor
from PyQt5.QtCore import pyqtSlot

from refTable import RefTable
from logValues import LogValues

class App(QWidget):
    def __init__(self, log, ref):
        super().__init__()
        self.title = 'AFR Viewer'
        self.left = 0
        self.top = 0
        self.width = 1600
        self.height = 1100

        self.log = log
        self.ref = ref

        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        self.createTable()

        # Add box layout, add table to box layout and add box layout to widget
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.tableWidget) 
        self.setLayout(self.layout) 

        # Show widget
        self.show()

    def createTable(self):
        self.tableWidget = QTableWidget()
        self.tableWidget.setRowCount(16)
        self.tableWidget.setColumnCount(16)

        header = []
        for item in self.ref.axisRPM:
            header.append(str(round(item)))
        self.tableWidget.setHorizontalHeaderLabels(header)

        header = []
        for item in self.ref.axisMAP:
            header.insert(0, str(round(item)))
        self.tableWidget.setVerticalHeaderLabels(header)

        for j in range(16):
            self.tableWidget.setColumnWidth(j, 6)
        
        self.fillTable()

    def fillTable(self):
        ref = self.ref
        log = self.log

        rpmIndex = log.titles.index("RPM")
        mapIndex = log.titles.index("MAP")
        mapdotIndex = log.titles.index("Accel Enrich")
        vetgtIndex = log.titles.index("PW Target")
        veIndex = log.titles.index("Fuel:PW")
        afrIndex = log.titles.index("AFR")
        corIndex = log.titles.index("Engine")

        for yIndex in range(1,len(ref.axisMAP)):
            for xIndex in range(len(ref.axisRPM)):
                xMin = ref.axisRPM[max(xIndex-1, 0)]
                xMax = ref.axisRPM[min(xIndex+1, 15)]
                yMin = ref.axisMAP[max(yIndex-1, 1)]
                yMax = ref.axisMAP[min(yIndex+1, 15)]
                xValue = ref.axisRPM[xIndex]
                yValue = ref.axisMAP[yIndex]

                xValues = []
                yValues = []
                zValues = []
                weight = 0
                weight2 = 0
                count = 0
                wcount = 0

                skip = 0
                for v in log.values:
                    rpmValue = v[rpmIndex]
                    mapValue = v[mapIndex]

                    if v[afrIndex] < 8 or v[rpmIndex] < 500 or int(v[corIndex]) & 0x0C:
                        continue

                    #skip transient rpm, lean or rich peak expected
                    if abs(v[mapdotIndex]) > 0:
                        #lower rpm react more
                        skip = 20 # 5000/rpmValue
                        continue

                    elif skip > 0:
                        skip -= 1
                        continue


                    if (rpmValue > xMin and rpmValue < xMax) and (mapValue > yMin and mapValue < yMax):
                        if rpmValue > xValue:
                            xD = (rpmValue-xValue) / (xMax-xValue)
                        else:
                            xD = (xValue-rpmValue) / (xValue-xMin)

                        if mapValue > ref.axisMAP[yIndex]:
                            yD = (mapValue-yValue) / (yMax-yValue)
                        else:
                            yD = (yValue-mapValue) / (yValue-yMin)

                        w = math.sqrt((xD**2 + yD**2) / 2)
                        distance = 1 / w

                        xValues.append(rpmValue)
                        yValues.append(mapValue)
                        zValues.append(v[vetgtIndex]*distance)

                        wcount += 1-w
                        count += 1
                        weight += distance
                        weight2 += distance * distance

                if weight > 0 and weight2/weight > 1 and yIndex > 0:
                    newitem = QTableWidgetItem(str(round(sum(zValues)/weight, 2)))
                    # color = int(min(200, math.sqrt(weight2)))
                    color = int(min(200, wcount))
                    newitem.setBackground(QColor(255-color, 255-color, 255))
                    newitem.setToolTip(str([count, wcount, weight, math.sqrt(weight2)]))
                else:
                    newitem = QTableWidgetItem(str(round(ref.refVE[yIndex][xIndex],2)))
                    newitem.setBackground(QColor(255, 200, 200))
                self.tableWidget.setItem(15-yIndex, xIndex, newitem)


if __name__ == '__main__':
    app = QApplication(sys.argv)

    log = LogValues()
    log.fill(sys.argv[1])

    ref = RefTable()
    if len(sys.argv) > 2:
        ref.fill(sys.argv[2])

    ex = App(log, ref)
    sys.exit(app.exec_())  