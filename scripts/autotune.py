#!/usr/bin/env python3

import sys
import math

from refTable import RefTable
from logValues import LogValues

#open datalog file
if len(sys.argv) < 2:
    print("Set file as argument")
    exit(0)

log = LogValues()
log.fill(sys.argv[1])

ref = RefTable()
if len(sys.argv) > 2:
    ref.fill(sys.argv[2])

rpmIndex = log.titles.index("RPM")
mapIndex = log.titles.index("MAP")
mapdotIndex = log.titles.index("Accel Enrich")
vetgtIndex = log.titles.index("PW Target")
veIndex = log.titles.index("Fuel:PW")
afrIndex = log.titles.index("AFR")


for yIndex in range(len(ref.axisMAP)):
    for xIndex in range(len(ref.axisRPM)):
        xMin = ref.axisRPM[max(xIndex-1, 0)]
        xMax = ref.axisRPM[min(xIndex+1, 15)]
        yMin = ref.axisMAP[max(yIndex-1, 0)]
        yMax = ref.axisMAP[min(yIndex+1, 15)]
        xValue = ref.axisRPM[xIndex]
        yValue = ref.axisMAP[yIndex]

        xValues = []
        yValues = []
        zValues = []
        wValues = []
        weight = 0
        weight2 = 0

        skip = 0
        for v in log.values:
            rpmValue = v[rpmIndex]
            mapValue = v[mapIndex]

            if v[afrIndex] < 8 or v[rpmIndex] < 500:
                continue

            #skip transient rpm, lean or rich peak expected
            if abs(v[mapdotIndex]) > 0:
                #lower rpm react more
                skip = 20 # 5000/rpmValue
                continue

            elif skip > 0:
                skip -= 1
                continue


            if (rpmValue > xMin and rpmValue < xMax) and (mapValue > yMin and mapValue < yMax):
                if rpmValue > xValue:
                    xD = (rpmValue-xValue) / (xMax-xValue)
                else:
                    xD = (xValue-rpmValue) / (xValue-xMin)

                if mapValue > ref.axisMAP[yIndex]:
                    yD = (mapValue-yValue) / (yMax-yValue)
                else:
                    yD = (yValue-mapValue) / (yValue-yMin)

                distance = 1 / math.sqrt((xD**2 + yD**2) / 2)

                xValues.append(rpmValue)
                yValues.append(mapValue)
                zValues.append(v[vetgtIndex]*distance)
                wValues.append(v[veIndex]*distance)
                weight += distance
                weight2 += distance * distance

        if weight > 0 and weight2/weight > 2:
            newVE = sum(zValues)/weight

            if ref.refVE:
                if abs(ref.refVE[yIndex][xIndex]-sum(wValues)/weight) < 2:
                    ref.refVE[yIndex][xIndex] = newVE

            else:
                print("{0}\t{1}\t{2}\t{3}\t{4}".format(
                    xValue,
                    yValue,
                    round(newVE,1),
                    round(sum(wValues)/weight,1),
                    round(weight2/weight,2))
                )

ref.export("output.table")