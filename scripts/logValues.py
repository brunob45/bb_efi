#!/usr/bin/env python3

class LogValues:
    def __init__(self):
        self.titles = []
        self.values = []
        pass

    def fill(self, filename:str):
        try:
            input_file = open(filename, "r")
        except:
            print("Set valid file as argument")
            return

        #skip header
        input_file.readline()
        input_file.readline()

        #parse titles
        titles_raw = input_file.readline()
        self.titles = titles_raw.strip().split('\t')

        #skip units
        input_file.readline()

        #parse values
        self.values = []
        for line in input_file:
            new_line = []
            for v in line.strip().split('\t'):
                new_line.append(float(v))
            self.values.append(new_line)

