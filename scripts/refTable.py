#!/usr/bin/env python3

import re

class RefTable:
    def __init__(self):
        self.axisRPM = [500, 800, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 8000]
        self.axisMAP = [12, 18, 24, 30, 36, 42, 48, 54, 60, 66, 72, 78, 84, 90, 96, 102]
        self.refVE = None
        self.refLines1 = None
        self.refLines2 = None

    def fill(self, filename:str):
        try:
            ref_file = open(filename, "r")
        except:
            return

        self.axisMAP = []
        self.axisRPM = []
        self.refVE = []
        self.refLines1 = []
        self.refLines2 = []

        #skip header
        for _ in range(5):
            self.refLines1.append(ref_file.readline())
        
        line = ref_file.readline()
        self.refLines1.append(line)
        xSize = int(re.search("cols=\"(\w+)\"", line).group(1))
        for _ in range(xSize):
            line = ref_file.readline()
            self.refLines1.append(line)
            self.axisRPM.append(float(line.strip()))
        self.refLines1.append(ref_file.readline())

        line = ref_file.readline()
        self.refLines1.append(line)
        ySize = int(re.search("rows=\"(\w+)\"", line).group(1))
        for _ in range(ySize):
            line = ref_file.readline()
            self.refLines1.append(line)
            self.axisMAP.append(float(line.strip()))
        self.refLines1.append(ref_file.readline())

        self.refLines1.append(ref_file.readline())
        for _ in range(ySize):
            row = []
            for item in ref_file.readline().strip().split(' '):
                row.append(float(item))
            self.refVE.append(row)
        
        for line in ref_file:
            self.refLines2.append(line)

    def export(self, filename:str):
        if self.refVE:
            output_file = open(filename, "w")
            for line in self.refLines1:
                output_file.write(line)

            for line in self.refVE:
                output_file.write("         ")
                for v in line:
                    output_file.write(str(round(v,2))+' ')
                output_file.write('\n')

            for line in self.refLines2:
                output_file.write(line)

