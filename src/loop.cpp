#include <Arduino.h>

#include "hardware.h"
#include "storage.h"
#include "tasks.h"

static void updateLoopCounter(const uint16_t& current_time)
{
    static uint16_t loop_counter = 0;
    static uint16_t last_update = 0;

    loop_counter++;
    if (current_time - last_update >= 1000)
    {
        current_status.loop_speed = loop_counter;
        loop_counter = 0;
        last_update = current_time;
        if (current_status.status1.run)
        {
            current_status.seconds++;
        }
    }
}

void loop()
{
    Context context;

    while (1)
    {
        io_led.set(!io_led.get());

        context.current_millis = short_millis();
        updateLoopCounter(context.current_millis);

        for (uint8_t task_index = 0; task_index < NUM_TASKS; task_index++)
        {
            context.current_task = task_index;
            if (tasks[task_index](context))
            {
                context.last_task = context.current_task;
                break;
            }
        }
    }
}
