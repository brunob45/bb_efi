#include <Arduino.h>

#include "auxPWM.h"
#include "comms.h"
#include "decoder.h"
#include "global_objects.h"
#include "hardware.h"
#include "schedule.h"
#include "sensors.h"
#include "vss.h"

void setup()
{
    io_led.setHigh();

    readStorage();

    // In case of tuning_settings corruption, only enable comms
    if (false)
    {
        commsInit();
        for (;;) { checkComms(); }
    }

    decoderInit();

    current_status.n_squirts = (tuning_settings.fuel.n_squirts > 0) ? tuning_settings.fuel.n_squirts : 1;
    current_status.teeth_per_event = 4 / current_status.n_squirts;

    sensorsInit();

    hardwareInit();
    commsInit();

    scheduleInit();
    auxPwmInit();
    vssInit();

    table_pw.multiplier = tuning_settings.fuel.req_fuel;
    table_cranking_fuel.multiplier = tuning_settings.fuel.req_fuel;

    io_led.setLow();

    interrupts();
}