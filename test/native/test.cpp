#include <unity.h>

#include "tests/test_calcs.h"
#include "tests/test_sensors.h"
#include "tests/test_states.h"
#include "tests/test_table.h"

int main()
{
    initSensors();
    initTable();
    initStates();
    initCalcs();

    UNITY_BEGIN();

    runSensors();
    runTable();
    runStates();
    runCalcs();

    UNITY_END();
}