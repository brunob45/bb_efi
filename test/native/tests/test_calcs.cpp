#include <unity.h>

#include "test_calcs.h"

#include "calcs.h"
#include "hardware.h"
#include "sensors.h"

void test_pw()
{
    const uint16_t OPEN_TIME = 0;
    uint16_t reqFuel = 12200;
    TEST_ASSERT_EQUAL(12200, getPW(reqFuel, 1000, 1, OPEN_TIME));
    TEST_ASSERT_EQUAL(6100, getPW(reqFuel, 1000, 2, OPEN_TIME));
    TEST_ASSERT_EQUAL(6100, getPW(reqFuel, 500, 1, OPEN_TIME));

    reqFuel = 25500;
    TEST_ASSERT_EQUAL(25500, getPW(reqFuel, 1000, 1, OPEN_TIME));
    TEST_ASSERT_EQUAL(25500, getPW(reqFuel, 2000, 2, OPEN_TIME));
    TEST_ASSERT_EQUAL(51000, getPW(reqFuel, 2000, 1, OPEN_TIME));
}

void test_max_pw()
{
    extern volatile tick_t last_trigger_delta_vg;

    last_trigger_delta_vg = 1000;
    TEST_ASSERT_EQUAL(3750, getMaxPw(1));

    last_trigger_delta_vg = 1000;
    TEST_ASSERT_EQUAL(7500, getMaxPw(2));

    last_trigger_delta_vg = 1000;
    TEST_ASSERT_EQUAL(15000, getMaxPw(4));

    last_trigger_delta_vg = 5000;
    TEST_ASSERT_EQUAL(37500, getMaxPw(2));

    last_trigger_delta_vg = 10000;
    TEST_ASSERT_EQUAL(65535, getMaxPw(2));
}

void test_afrtgt()
{
    int16_t axis_values[] = {0};
    uint8_t values[] = {0};
    TableAxis axis = {1, axis_values, 0, 0};
    Table3d table = {&axis, &axis, values, 1};
    axis.Update(0);

    values[0] = 128;
    TEST_ASSERT_EQUAL(EGOTGT_VALUE(1.0), getAfrTarget(&table));

    values[0] = 0;
    TEST_ASSERT_EQUAL(EGOTGT_VALUE(0.75), getAfrTarget(&table));

    values[0] = 255;
    TEST_ASSERT_EQUAL(EGOTGT_VALUE(1.249), getAfrTarget(&table));

    TEST_ASSERT_EQUAL(10000, applyAfrTarget(10000, EGOTGT_VALUE(1.0)));
    TEST_ASSERT_EQUAL(12518, applyAfrTarget(10000, EGOTGT_VALUE(0.8)));
    TEST_ASSERT_EQUAL(8338, applyAfrTarget(10000, EGOTGT_VALUE(1.2)));
}

void initCalcs()
{
}

void runCalcs()
{
    RUN_TEST(test_pw);
    RUN_TEST(test_max_pw);
    RUN_TEST(test_afrtgt);
}