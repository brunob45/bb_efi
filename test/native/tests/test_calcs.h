#if !defined(TEST_CALCS_H)
#define TEST_CALCS_H

void initCalcs();
void runCalcs();

#endif // TEST_CALCS_H