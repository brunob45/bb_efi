#include "test_sensors.h"

#include <unity.h>

#include "sensors.h"
#include "storage.h"

void initSensors()
{
    tuning_settings.calib.brv_axis[0] = 32;
    tuning_settings.calib.brv_axis[1] = 992;
    tuning_settings.calib.brv_values[0] = 0;
    tuning_settings.calib.brv_values[1] = 150;

    tuning_settings.calib.map_axis[0] = 0;
    tuning_settings.calib.map_axis[1] = 1023;
    tuning_settings.calib.map_values[0] = 5;
    tuning_settings.calib.map_values[1] = 130;

    tuning_settings.calib.tps_axis[0] = 32;
    tuning_settings.calib.tps_axis[1] = 992;
    tuning_settings.calib.tps_values[0] = 0;
    tuning_settings.calib.tps_values[1] = 100;

    tuning_settings.calib.ego_axis[0] = 0;
    tuning_settings.calib.ego_axis[1] = 1023;
    tuning_settings.calib.ego_values[0] = 0.5 * 128;
    tuning_settings.calib.ego_values[1] = 1.5 * 128;
}

void runSensors()
{
    RUN_TEST(sensor_brv);
    RUN_TEST(sensor_map);
    RUN_TEST(sensor_tps);
    RUN_TEST(sensor_ego);
}

void sensor_brv()
{
    TEST_ASSERT_EQUAL(BRV_VALUE(0), getBrvValue(0));

    TEST_ASSERT_EQUAL(BRV_VALUE(0), getBrvValue(32));

    TEST_ASSERT_EQUAL(BRV_VALUE(1.0), getBrvValue(64));

    TEST_ASSERT_EQUAL(BRV_VALUE(29.0), getBrvValue(960));

    TEST_ASSERT_EQUAL(BRV_VALUE(30), getBrvValue(992));

    TEST_ASSERT_EQUAL(BRV_VALUE(30), getBrvValue(1023));
}

void sensor_map()
{
    TEST_ASSERT_EQUAL(MAP_VALUE(10), getMapValue(0));

    TEST_ASSERT_EQUAL(MAP_VALUE(72.3), getMapValue(255));

    TEST_ASSERT_EQUAL(MAP_VALUE(134.8), getMapValue(511));

    TEST_ASSERT_EQUAL(MAP_VALUE(197.4), getMapValue(767));

    TEST_ASSERT_EQUAL(MAP_VALUE(260), getMapValue(1023));
}

void sensor_tps()
{
    TEST_ASSERT_EQUAL(TPS_VALUE(0), getTpsValue(0));

    TEST_ASSERT_EQUAL(TPS_VALUE(23), getTpsValue(255));

    TEST_ASSERT_EQUAL(TPS_VALUE(49), getTpsValue(511));

    TEST_ASSERT_EQUAL(TPS_VALUE(76), getTpsValue(767));

    TEST_ASSERT_EQUAL(TPS_VALUE(100), getTpsValue(1023));
}

void sensor_ego()
{
    TEST_ASSERT_EQUAL(EGO_VALUE(0.5), getEgoValue(0));

    TEST_ASSERT_EQUAL(EGO_VALUE(0.7491), getEgoValue(255));

    TEST_ASSERT_EQUAL(EGO_VALUE(0.9991), getEgoValue(511));

    TEST_ASSERT_EQUAL(EGO_VALUE(1.2491), getEgoValue(767));

    TEST_ASSERT_EQUAL(EGO_VALUE(1.5), getEgoValue(1023));
}
