#if !defined(TEST_SENSORS_H)
#define TEST_SENSORS_H

void initSensors();
void runSensors();

void sensor_brv();
void sensor_map();
void sensor_tps();
void sensor_ego();

#endif // TEST_SENSORS_H