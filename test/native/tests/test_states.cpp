#include <unity.h>

#include "global_objects.h"
#include "state_machine.h"
#include "storage.h"
#include "test_states.h"

void states_stop2run()
{
    current_status.engine.ready = true;
    current_status.rpm = 1000;
    current_status.status1.run = false;
    current_status.engine.crank = false;

    updateEngineState();
    TEST_ASSERT_TRUE(current_status.status1.run);
    TEST_ASSERT_FALSE(current_status.engine.crank);
}

void states_stop2cranking()
{
    current_status.engine.ready = true;
    current_status.rpm = 200;
    current_status.status1.run = false;
    current_status.engine.crank = false;

    updateEngineState();
    TEST_ASSERT_TRUE(current_status.status1.run);
    TEST_ASSERT_TRUE(current_status.engine.crank);
}

void states_cranking2run()
{
    current_status.engine.ready = true;
    current_status.rpm = 1000;
    current_status.status1.run = true;
    current_status.engine.crank = true;
    tuning_settings.fuel.n_squirts = 2;

    updateEngineState();
    TEST_ASSERT_TRUE(current_status.status1.run);
    TEST_ASSERT_FALSE(current_status.engine.crank);
}

void states_cranking2stop()
{
    current_status.engine.ready = false;
    current_status.rpm = 1000;
    current_status.status1.run = true;
    current_status.engine.crank = true;

    updateEngineState();
    TEST_ASSERT_FALSE(current_status.status1.run);
    TEST_ASSERT_FALSE(current_status.engine.crank);
}

void states_run2stop()
{
    current_status.engine.ready = false;
    current_status.rpm = 1000;
    current_status.status1.run = true;
    current_status.engine.crank = false;

    updateEngineState();
    TEST_ASSERT_FALSE(current_status.status1.run);
    TEST_ASSERT_FALSE(current_status.engine.crank);
}

void initStates()
{
    tuning_settings.spark.cranking_rpm = 20; // 400 RPM
    tuning_settings.fuel.ase_taper = 15;
}

void runStates()
{
    RUN_TEST(states_stop2run);
    RUN_TEST(states_stop2cranking);

    RUN_TEST(states_run2stop);

    RUN_TEST(states_cranking2run);
    RUN_TEST(states_cranking2stop);
}