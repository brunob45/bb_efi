#if !defined(TEST_STATES_H)
#define TEST_STATES_H

void initStates();
void runStates();

#endif // TEST_STATES_H