#include "test_table.h"

#include <unity.h>

#include "table.h"

static int16_t axis_bins[] = {0, 10, 20, 30};

static TableAxis axis_x = {4, axis_bins, 0, 0};
static TableAxis axis_y = {4, axis_bins, 0, 0};

static uint8_t values[] = {0, 10, 5, 15,
                           10, 20, 15, 25,
                           5, 15, 10, 20,
                           15, 25, 20, 30};

static Table2d table2 = {
    &axis_x,
    values,
    10};
static Table3d table3 = {
    &axis_x,
    &axis_y,
    values,
    5};

void initTable()
{
}

void runTable()
{
    RUN_TEST(tableAxis);
    RUN_TEST(table2d);
    RUN_TEST(table3d);
}

void tableAxis()
{
    axis_x.Update(-1);
    TEST_ASSERT_EQUAL(0, axis_x.bin);
    TEST_ASSERT_EQUAL(0, axis_x.w);

    axis_x.Update(1);
    TEST_ASSERT_EQUAL(0, axis_x.bin);
    TEST_ASSERT_EQUAL(25, axis_x.w);

    axis_x.Update(10);
    TEST_ASSERT_EQUAL(1, axis_x.bin);
    TEST_ASSERT_EQUAL(0, axis_x.w);

    axis_x.Update(15);
    TEST_ASSERT_EQUAL(1, axis_x.bin);
    TEST_ASSERT_EQUAL(128, axis_x.w);

    axis_x.Update(19);
    TEST_ASSERT_EQUAL(1, axis_x.bin);
    TEST_ASSERT_EQUAL(230, axis_x.w);

    axis_x.Update(31);
    TEST_ASSERT_EQUAL(3, axis_x.bin);
    TEST_ASSERT_EQUAL(0, axis_x.w);
}

void table2d()
{
    int16_t v;

    axis_x.Update(10);
    v = table2.getValue();
    TEST_ASSERT_EQUAL(100, v);

    axis_x.Update(15);
    v = table2.getValue();
    TEST_ASSERT_EQUAL(75, v);

    axis_x.Update(25);
    v = table2.getValue();
    TEST_ASSERT_EQUAL(100, v);
}

void table3d()
{
    int16_t v;

    axis_x.Update(10);
    axis_y.Update(10);
    v = table3.getValue();
    TEST_ASSERT_EQUAL(100, v);

    axis_x.Update(15);
    axis_y.Update(10);
    v = table3.getValue();
    TEST_ASSERT_EQUAL(88, v);

    axis_x.Update(10);
    axis_y.Update(15);
    v = table3.getValue();
    TEST_ASSERT_EQUAL(88, v);

    axis_x.Update(15);
    axis_y.Update(15);
    v = table3.getValue();
    TEST_ASSERT_EQUAL(76, v);

    axis_x.Update(25);
    axis_y.Update(25);
    v = table3.getValue();
    TEST_ASSERT_EQUAL(100, v);
}