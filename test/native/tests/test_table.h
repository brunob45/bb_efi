#if !defined(TEST_TABLE_H)
#define TEST_TABLE_H

void initTable();
void runTable();

void tableAxis();
void table2d();
void table3d();

#endif // TEST_TABLE_H